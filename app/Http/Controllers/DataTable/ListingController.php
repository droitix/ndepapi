<?php

namespace App\Http\Controllers\DataTable;

use App\{Area, Category, Listing};
use Illuminate\Http\Request;
use App\Http\Controllers\DataTable\DataTableController;

class ListingController extends DataTableController
{
  

    public function builder()
    {
        return Listing::query();
    }

    public function getDisplayableColumns()
    {
        return [
            'id',
            'user_id',
            'address',
            'phone1',
            'phone2',
            'phone3',
            'companyemail',
            'companyname',
            'facebook',
            'twitter',
            'instagram',
            'title',
           
        ];
    }


     public function getUpdatableColumns()
    {
        return [
            'user_id',
            'address',
            'phone1',
            'phone2',
            'phone3',
            'companyemail',
            'companyname',
            'facebook',
            'twitter',
            'instagram',
            'title',
        ];
    }

      public function update($id, Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'companyname' => 'required',
         
        ]);
           
        $this->builder->find($id)->update($request->only($this->getUpdatableColumns()));
    }

}
