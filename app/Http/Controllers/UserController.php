<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

class UserController extends Controller
{
    public function profile()
  {
    return view('profile.profile', array('user' => Auth::user()));
  }
   public function update_avatar(Request $request)
  {
      if($request->hasFile('avatar')){
        $avatar = $request->file('avatar');
        $filename = time() . '.' . $avatar->getClientOriginalExtension();
        Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' .$filename) );

        $user = Auth::user();
        $user->avatar = $filename;
        $user->save();
      }
      return redirect()->back()->withInput();
  }
    public function update(Request $request)
  {  
    $request = $request->validate([
      'name' => 'required',
      'email' => 'required',
      'fullname' => 'required',
      'phone' => '',
      'facebook' => '',
      'twitter' => '',
      'instagram' => '',
      'location' => '',
     
     
    ]);

    $saved = false;
    $user = auth()->user();
    $user->name = $request['name'];
    $user->fullname = $request['fullname'];
    $user->phone = $request['phone'];
    $user->email = $request['email'];
    $user->facebook = $request['facebook'];
    $user->twitter = $request['twitter'];
    $user->instagram = $request['instagram'];
    $user->location = $request['location'];


    if($user->save()) $saved = true;

    return view('profile.profile', compact('saved', 'user'));
  }

}
