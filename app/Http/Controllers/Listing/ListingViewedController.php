<?php

namespace App\Http\Controllers\Listing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ListingViewedController extends Controller
{
    const INDEX_LIMIT = 10;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
            $user = Auth::user();

        $listings = $request->user()
            ->viewedListings()->with(['area', 'user'])
            ->orderByPivot('updated_at', 'desc')
            ->isLive()
            ->take(self::INDEX_LIMIT)
            ->get();

        return view('user.listings.viewed.v2index', [
            'listings' => $listings,
            'user'=>$user,
            'indexLimit' => self::INDEX_LIMIT,
        ]);
    }
}
