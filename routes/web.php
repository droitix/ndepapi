<?php

                 Route::get('/', 'HomeController@index');








                 Auth::routes();
                 
                 Auth::routes(['verify' => true]);
    Route::delete('/admin/impersonate', 'Admin\ImpersonateController@destroy');
Route::group(['middleware' => 'role:admin'], function () {
                 Route::get('admin/users', 'Admin\UserController@index');
                  Route::get('admin/plans', 'Admin\PlanController@index');
                   Route::get('admin/listings', 'Admin\ListingController@index');

                    Route::get('/admin/impersonate', 'Admin\ImpersonateController@index')->name('admin.impersonate');
                   Route::post('/admin/impersonate', 'Admin\ImpersonateController@store');



                 Route::resource('datatable/users', 'DataTable\UserController');
                 Route::resource('datatable/plans', 'DataTable\PlanController');
                 Route::resource('datatable/listings', 'DataTable\ListingController');
});
                 Route::post('/upload-image/{userId}', 'ImagesController@store');

                 Route::get('/user/area/{area}', 'User\AreaController@store')->name('user.area.store');

                 Route::get('/braintree/token', 'BraintreeController@token');

                 Route::get('profile', 'UserController@profile')->name('profile')->middleware('auth');

                 Route::get('/search', 'Listing\ListingController@search')->name('search');


                 Route::post('/profile/update-avatar', 'UserController@update_avatar')->middleware('auth')->name('profile.update.avatar');
                 Route::post('/profile', 'UserController@update')->middleware('auth')->name('profile.update');
Route::group(['prefix' => '/{area}'], function () {

    /**
     * Category
     */
    Route::group(['prefix' => '/categories'], function () {
                Route::get('/', 'Category\CategoryController@index')->name('category.index');

        Route::group(['prefix' => '/{category}'], function () {
                Route::get('/listings', 'Listing\ListingController@index')->name('listings.display.index3');
       
        });
    });

    /**
     * Listings
     */
    Route::group(['prefix' => '/listings', 'namespace' => 'Listing'], function () {
        Route::get('/same-category/{category}', 'ListingPublishedController@listingsInCategory')->name('listings.published.index');

        Route::get('/favourites', 'ListingFavouriteController@index')->name('listings.favourites.index');

        Route::post('/{listing}/favourites', 'ListingFavouriteController@store')->name('listings.favourites.store');
        Route::delete('/{listing}/favourites', 'ListingFavouriteController@destroy')->name('listings.favourites.destroy');

        Route::get('/viewed', 'ListingViewedController@index')->name('listings.viewed.index')->middleware('auth');


        Route::post('/{listing}/contact', 'ListingContactController@store')->name('listings.contact.store');

        Route::get('/{listing}/payment', 'ListingPaymentController@show')->name('listings.payment.show');
        Route::post('/{listing}/payment', 'ListingPaymentController@store')->name('listings.payment.store');
        Route::patch('/{listing}/payment', 'ListingPaymentController@update')->name('listings.payment.update');

        Route::get('/unpublished', 'ListingUnpublishedController@index')->name('listings.unpublished.index');


        Route::get('/published', 'ListingPublishedController@index')->name('listings.published.index');


        Route::get('/{listing}/share', 'ListingShareController@index')->name('listings.share.index');
        Route::post('/{listing}/share', 'ListingShareController@store')->name('listings.share.store');

        Route::group(['middleware' => 'auth'], function () {
            Route::get('/create', 'ListingController@create')->name('listings.create');
            Route::post('/', 'ListingController@store')->name('listings.store');

            Route::get('/{listing}/edit', 'ListingController@edit')->name('listings.edit');
            Route::patch('/{listing}', 'ListingController@update')->name('listings.update');
            Route::delete('/{listing}', 'ListingController@destroy')->name('listings.destroy');
        });
    });

    Route::get('/{listing}', 'Listing\ListingController@show')->name('listings.show');

});

Route::get('/about', 'StaticPagesController@about');
Route::get('/test', 'StaticPagesController@test');

Route::get('/faq', 'StaticPagesController@faq');
Route::get('/privacy', 'StaticPagesController@privacy');
Route::get('/terms', 'StaticPagesController@terms');
Route::get('/contact', 'StaticPagesController@contact')->name('Pages.contact');
Route::post('/contact', 'StaticPagesController@store')->name('contact.send');
