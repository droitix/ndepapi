<main>
        <div id="results">
           <div class="container">
               <div class="row">
                   <div class="col-lg-3 col-md-4 col-10">
                       <h4>Currently <strong><?php echo count($category->listings) ?></strong> {{ $category->name }} are listed in {{ $area->name }}</h4>
                   </div>
                   <div class="col-lg-9 col-md-8 col-2">
                       <a href="#0" class="side_panel btn_search_mobile"></a> <!-- /open search panel -->
                       <form method="GET" action="{{route('search')}}">
                         {{ csrf_field() }}
                        <div class="row no-gutters custom-search-input-2 inner">
                            <div class="col-lg-11">
                                <div class="form-group">
                                    <input class="form-control" name="query" type="text" placeholder="Search for more {{ $category->name }} in {{ $area->name }} .  .  .  .">
                                   <i class="fas fa-search"></i>
                                </div>
                            </div>
                           
                           
                            <div class="col-lg-1">
                                <input type="submit" value="Search">
                            </div>
                        </div>
                        </form>
                   </div>
               </div>
               <!-- /row -->
           </div>
           <!-- /container -->
       </div>
        
        <!-- /results -->       