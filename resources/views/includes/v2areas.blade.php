  
        <div class="main_categories">
            <div class="container">

                 @foreach ($areas as $country)
                <ul class="clearfix">
                    <div class="areas">
                        <span><em></em></span>
                    <h4 class="areas" style="margin-bottom: 2vw;">Select an area and browse all the categories.</h4>
                    </div>
                     @foreach ($country->children as $state)
                    <li>
                        <a href="{{ route('user.area.store', $state) }}">
                            <i class="fas {{ $state->icon }}" style="color:#{{ $state->color }};"></i>
                            <h3>{{ $state->name }}</h3>
                        </a>
                    </li>
                      @endforeach
                </ul>
                @endforeach
            </div>
            <!-- /container -->
        </div>
        <!-- /main_categories -->