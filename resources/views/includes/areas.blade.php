 <div class="tr-category section-padding">
        <div class="container">
            <div class="section-title">
                <h1>Select an area and enjoy all the categories in it.</h1>

            </div>

            @foreach ($areas as $country)
            <button btn btn-primary><a href="{{ route('user.area.store', $country) }}">Reset to {{ $country->name }}</a></button>
            
            <ul class="category-list tr-list">
                 @foreach ($country->children as $state)

                <li>
                              
                   <a href="{{ route('user.area.store', $state) }}">
                        <span class="icon">
                            <i class="fas {{ $state->icon }} fa-3x" style="color:#{{ $state->color }};"></i>
                        </span>
                        <span class="category-title">{{ $state->name }}</span>
                        <span class="category-quantity"></span>
                      

                    </a>

                </li>
                @endforeach
            </ul>
            @endforeach
        </div><!-- /.container -->
    </div><!-- /.tr-category -->

    

