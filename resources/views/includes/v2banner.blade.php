
<section class="banner_sec">
<section class="hero_single version_2">
            <div class="wrapper">
                <div class="container">
                    <h3>Let {{ $area->name }} know what you are doing!</h3>
                    <p>Discover hotels, lodges , schools, restaurants, events in your locality and throughout zimbabwe</p>
                    <form method="GET" action="{{route('search')}}">
                         {{ csrf_field() }}
                        <div class="row no-gutters custom-search-input-2">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <input class="form-control" name="query" type="text" placeholder="Search listings by title, body, location etc . . . . .">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                            
                            
                            <div class="col-lg-2">
                                <input type="submit" value="Search">
                            </div>
                        </div>
                        <!-- /row -->
             </form>
                </div>
            </div>
        </section>
        <!-- /hero_single -->
        </section>