@extends('layouts.v2authapp')

@section('content')
  <body id="login_bg">
    
    <nav id="menu" class="fake_menu"></nav>
    
    <div id="login">
        <aside>
            <figure>
                <a href="{{ url('/') }}"><img src="/svg/logo.png" width="125" height="35" alt="" class="logo_sticky"></a>
            </figure>
            <form method="POST" class="tr-form" action="{{ route('login') }}">
                               @csrf
              
                <div class="divider"><span>Or</span></div>
                <div class="form-group">
                    <label>Email</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>   <i class="fas fa-envelope"></i>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                 
                </div>
                <div class="form-group">
                    <label>Password</label>
                   <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    <i class="fas fa-lock"></i>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                  
                </div>
                <div class="clearfix add_bottom_30">
                    <div class="checkboxes float-left">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                    </div>
                     <div class="float-right mt-1"><a  href="{{ route('password.request') }}">Forgot Password?</a></div>
                    <button type="submit" class="btn_1 rounded full-width" >
                                    {{ __('Login') }}
                                </button>
                   
                </div>
                
                <div class="text-center add_top_10">New to Ndepapi? <strong><a href="{{ url('/register') }}">Sign up!</a></strong></div>
            </form>
            <div class="copy">© 2018 Ndepapi</div>
        </aside>
    </div>
    <!-- /login -->

@endsection