<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta property="og:url"           content="{{ route('listings.show', [$area, $listing]) }}" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="{{ $listing->companyname }}" />
  <meta property="og:description"   content="{{ $listing->body }}" />
  <meta property="og:image"         content="{{'/storage/'.$image}}" />
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130589086-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-130589086-1');
</script>
    @include('layouts.partials.headclass')
    @yield('edit-page-styles-scripts')
</head>
<body>
    <div id="appclass">
        @include('layouts.partials.v2navigationlist')
       

               @include('layouts.partials._alerts')
            @yield('content') 
            
            
        <
    </div>
    @include('maps-modal')
     @include('layouts.partials.v2footer')

    <!-- JS -->
    <script src="/public/js/jquery.min.js"></script>
    <script src="/public/js/modernizr.min.js"></script>
    <script src="/public/js/bootstrap.min.js"></script>
    <!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
    <!-- <script src="/public/js/gmaps.min.js"></script> -->
    <script src="/public/js/owl.carousel.min.js"></script>
    <script src="/public/js/smoothscroll.min.js"></script>
    <script src="/public/js/scrollup.min.js"></script>
    <script src="/public/js/price-range.js"></script>  
    <script src="/public/js/jquery.countdown.js"></script>  
    <script src="/public/js/custom.js"></script>
    <script src="/public/js/switcher.js"></script>

    @yield('more-scripts') 
    

</body>
</html>
