<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.v2head')
 
</head>
<body>
 
          <div id="app">
  


    
       @yield('content')



  
           </div>
     


   
    
    <!-- COMMON SCRIPTS -->
    <script src="/2.0/js/common_scripts.js"></script>
    <script src="/2.0/js/functions.js"></script>
    <script src="/2.0/assets/validate.js"></script>
    
    <!-- SPECIFIC SCRIPTS -->
    <script src="/2.0/js/animated_canvas_min.js"></script>   
</body>
</html>
