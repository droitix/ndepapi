<!DOCTYPE html>
<html lang="en">
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
    <meta name="description" content="">

    <title>Ndepapi | Country-wide listing</title>
    
    
  

   <!-- CSS -->
    <link rel="stylesheet" href="/public/css/bootstrap.min.css" >
    <link rel="stylesheet" href="/public/css/font-awesome.min.css">
    <link rel="stylesheet" href="/public/css/icofont.css">
    <link rel="stylesheet" href="/public/css/owl.carousel.css">  
    <link rel="stylesheet" href="/public/css/slidr.css">     
    <link rel="stylesheet" href="/public/css/main.css">  
   
    <link rel="stylesheet" href="/public/css/responsive.css">
    
    
    <!-- font -->

    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

     <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- icons -->
   <link rel="icon" href="/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/public/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/public/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/public/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/public/images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
 

       <!-- JS -->
    <script src="/public/js/jquery.min.js"></script>
    <script src="/public/js/modernizr.min.js"></script>
    <script src="/public/js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="/public/js/gmaps.min.js"></script>
    <script src="/public/js/owl.carousel.min.js"></script>
    <script src="/public/js/smoothscroll.min.js"></script>
    <script src="/public/js/scrollup.min.js"></script>
    <script src="/public/js/price-range.js"></script>  
    <script src="/public/js/jquery.countdown.js"></script>  
    <script src="/public/js/custom.js"></script>
    <script src="/public/js/switcher.js"></script>
    
   
