 <header class="tr-header">
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/')}}"><img class="img-responsive" height="102" width="102" src="/images/logo.png" alt="Logo"></a>
                </div>
                <!-- /.navbar-header -->
                
                

                <div class="navbar-right">          
                      @guest               
                    <ul class="sign-in tr-list">
                        <li><i class="fa fa-user"></i></li>
                        <li><a href="{{ route('login'). '?previous=' . Request::fullUrl()  }}">Sign In </a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    </ul><!-- /.sign-in -->                 
                    

                    <a href="{{ route('listings.create', [$area]) }}" class="btn btn-primary">New listing</a>
                     @else

                      <ul class="sign-in tr-list" >
                      @if (session()->has('impersonate'))
                        <li>
                            <a href="#" onclick="event.preventDefault(); document.getElementById('impersonating').submit();">Stop Impersonating</a>
                        </li>
                        <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>

                      @endif
                          
                    
                        
                                
                                                 
                            <li><img src="/uploads/avatars/{{ Auth::user()->avatar }}" alt="" class="img-responsive" style="width :32px; height:32px; position:relative; border-radius: 50%; top:10px; left:10px;"></li>
                      
                    </ul>
                      
                            </li>
                         <ul class="nav navbar-nav">
                                        
 
                            <li class="tr-dropdown"><a href="#">Hello, {{ Auth::user()->name }}<span class="caret"></span></a>
                                <ul class="tr-dropdown-menu left tr-list fadeInUp" role="menu">
                                    <li><a href="{{ route('profile') }}"><i class="fas fa-cog"></i> My Profile</a></li>
                                     <li><a href="{{ route('listings.published.index', [$area]) }}"><i class="fas fa-clipboard-list"></i> My Listings({{ $publishedListingsCount }})</a></li>
                            <li><a href="{{ route('listings.unpublished.index', [$area]) }}"><i class="fas fa-drafting-compass"></i> My Drafts({{ $unpublishedListingsCount }})</a></li>
                            <hr style="margin-top: 0.5em; margin-bottom: 0.5em; margin-left: 1em; margin-right: 1em;">
                            <li><a href="{{ route('listings.viewed.index', [$area]) }}"><i class="fas fa-bullseye"></i> Recently viewed</a></li>

                            <li><a href="{{ route('listings.favourites.index', [$area]) }}"><i class="fas fa-thumbtack"></i> Favourites</a></li>
                               @role('admin')
                             <li><a href="{{ url('admin/listings') }}"><i class="fas fa-cog"></i>Admin listings</a></li>
                              <li><a href="{{ url('admin/users')}}"><i class="fas fa-cog"></i>Admin users</a></li>
                              <li><a href="{{ url('admin/impersonate')}}"><i class="fas fa-cog"></i>Impersonate</a></li>
                               @endrole

                                    <li>   <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form></li>
                                </ul>  
                            
                        </ul> 

                     <a href="{{ route('listings.create', [$area]) }}" class="btn btn-primary">New listing</a>
                      
                     @endguest

                 
                               
                </div><!-- /.nav-right -->

            </div><!-- /.container -->
        </nav><!-- /.navbar -->
    </header><!-- /.tr-header -->