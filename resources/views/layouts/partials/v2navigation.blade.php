<header class="header menu_fixed">
        <div id="logo">
            <a href="{{ url('/') }}" title="Sparker - Directory and listings template">
                <img src="/images/logo.png" width="125" height="35" alt="" class="logo_normal">
                <img src="/images/logo.png" width="125" height="35" alt="" class="logo_sticky">
            </a>
        </div>
        <ul id="top_menu">

                        @guest

                        <li><a href="#sign-in-dialog" id="sign-in" title="Sign In"><img src="/svg/login-2.svg" width="25" height="35" alt=""></a></li>
                         <li><a href="{{ url('/login') }}" id="sign-in" class="btn_add"><i class="fas fa-plus-circle"></i> Add Listing</a></li>
                         
                         @else
                         <li><a href="{{ route('listings.create', [$area]) }}" class="btn_add"><i class="fas fa-plus-circle"></i>Add Listing</a></li>
                        @endguest
        </ul>
        <!-- /top_menu -->
        <a href="#menu" class="btn_mobile">
            <div class="hamburger hamburger--spin" id="hamburger">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
        </a>
        <nav id="menu" class="main-menu">
            <ul>
                 @guest

                @else
                    <li><img src="/uploads/avatars/{{ Auth::user()->avatar }}" alt="" class="img-responsive" style="width :32px; height:32px; position:relative; border-radius: 50%; top:0px; left:0px;"></li>
                <li><span><a href="#0"> Hello, {{ Auth::user()->name }}<span class="caret"></span></a></span>
                    <ul>
                <li><a href="{{ route('profile') }}"><i class="fas fa-cog"></i> My Profile</a></li>
                        <li><a href="{{ route('listings.published.index', [$area]) }}"><i class="fas fa-clipboard-list"></i> My Listings({{ $publishedListingsCount }})</a></li>
                       <li><a href="{{ route('listings.unpublished.index', [$area]) }}"><i class="fas fa-drafting-compass"></i> My Drafts({{ $unpublishedListingsCount }})</a></li>
                       <li><a href="{{ route('listings.viewed.index', [$area]) }}"><i class="fas fa-bullseye"></i> Recently viewed</a></li>
                        <li><a href="{{ route('listings.favourites.index', [$area]) }}"><i class="fas fa-bookmark"></i> Favourites</a></li>
                        <li>   <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form></li>
                     @endguest          
            </ul>
        </nav>
    </header>
    <!-- /header -->