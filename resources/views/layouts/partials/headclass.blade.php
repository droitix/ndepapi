<!DOCTYPE html>
<html lang="en">
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta property="og:url"           content="{{ route('listings.show', [$area, $listing]) }}" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="{{ $listing->companyname }}" />
  <meta property="og:description"   content="{{ $listing->body }}" />
  <meta property="og:image"         content="{{'/storage/'.$image}}" />
    <title>{{ $listing->companyname }} | Ndepapi listings</title>

  <link rel="shortcut icon" href="/svg/favicon.ico" type="image/x-icon">
     <!-- BASE CSS -->
    <link href="/2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="/2.0/css/style2.css" rel="stylesheet">
    <link href="/2.0/css/vendors.css" rel="stylesheet">
    <link href="/2.0/css/custom.css" rel="stylesheet">
    
  

   <!-- CSS -->
    <link rel="stylesheet" href="/public/css/bootstrap.min.css" >
  
   
    <link rel="stylesheet" href="/public/css/owl.carousel.css">  
    <link rel="stylesheet" href="/public/css/slidr.css">     
    <link rel="stylesheet" href="/public/css/mainshow.css">  
   
    <link rel="stylesheet" href="/public/css/responsive.css">
    
    
    <!-- font -->



     <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    

   
