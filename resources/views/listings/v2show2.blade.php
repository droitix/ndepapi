@extends('layouts.v2applist')

@section('content')
    <main>  
@include('includes.v2bannershow')
<div class="row">   
  <div class="box_account col-md-6" style="padding-left: 20px; padding-top: 10px; border: 0px solid;" >
   
      <div class="slid_container">
 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    @if($listing->images != '')
    @foreach($listing->images as $key => $image)
<li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}" @if($key == 0) class="active" @endif></li>
   
     @endforeach

                           
    @endif
  </ol>
  <div class="carousel-inner"  style="max-height: 400px">
     
       @foreach($listing->images as $key => $image)
    <div class="carousel-item @php if($key == 0) echo 'active'; @endphp">
      <img class="d-block w-100" src="{{'/storage/'.$image}}" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
   <span class="magnific-gallery">
        @foreach($listing->images as $image)
                    <a href="{{'/storage/'.$image}}" class="btn_photos" title="Photo title" data-effect="mfp-zoom-in">@endforeach <button btn-primary>View photos</button></a>
               
      </span>
  </div>
    </div>
   
     @endforeach
      
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
</div>


    <div class="col-lg-5 col-md-5" style="margin-top:2vw; margin-left: 4vw;">
                    <div class="step first">
                        <h3>{{ $listing->companyname }}</h3>
                    <div class="box_general summary">
                        
                        <ul>
                            <li><i class="fas fa-map-marker-alt" style="color:#26B13E;"></i> Find us here <span class="float-right">{{ $listing->address }}</span></li>
                            <li><i class="fas fa-phone" style="color:#26B13E;"></i> Call us <span class="float-right">{{ $listing->phone1 }}</span></li>
                            <li><i class="fas fa-blog" style="color:#26B13E;"></i> Visit our site <span class="float-right">{{ $listing->website }}</span></li>
                            <li><i class="fas fa-clipboard-list"></i> Listed since <span class="float-right">{{ $listing->created_at }}</span></li>
                           
                        </ul>
                       
                        
                        
                        <button id="show-address" class="btn_1 full-width outline wishlist"> <i class="fa fa-map-marker-alt" data-toggle="modal" 
                                    data-target="#mapModal" data-address="{{$listing->address}}"
                                    data-place-name="{{$listing->companyname}}"
                                    data-category="{{$listing->category->id}}"
                                    data-area="{{$area->slug}}"> View on Map</i> 
                         </button> 
                    </div>
                    <!-- /box_general -->
                    </div>
                    <!-- /step -->
                </div>

</div>

     

        <div class="container margin_60_35">
                <div class="row">
                    <div class="col-lg-8">
                        <section id="description">
                            <div class="detail_title_1">
                                <h1>{{ $listing->companyname }}</h1>
                                <a class="address" href="#">{{ $listing->address }}</a>
                            </div>
                            <p>{{ $listing->body }}</p>
                          
                                         
                            <div class="opening">
                                
                                <i class="icon_clock_alt"></i>
                                <h4>Our Opening Hours</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul>
                                            <li>Monday <span>{{ $listing->monopening }} - {{ $listing->monclosing }}</span></li>
                                            <li>Tuesday <span>{{ $listing->tuesopening }} - {{ $listing->tuesclosing }}</span></li>
                                            <li>Wednesday <span>{{ $listing->wedopening }} - {{ $listing->wedclosing }}</span></li>
                                            <li>Thursday <span>{{ $listing->thursopening }} - {{ $listing->thursclosing }}</span></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul>
                                            <li>Friday <span>{{ $listing->fridayopening }} - {{ $listing->fridayclosing }}</span></li>
                                            <li>Saturday <span>{{ $listing->surtopening }} - {{ $listing->surtclosing }}</span></li>
                                            <li>Sunday <span>{{ $listing->sunopening }} - {{ $listing->sunclosing }}</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /opening -->
                            <hr>
                           
                 

                            <div class="add-review">
                                <h5>Leave a Review</h5>
                                <form>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Name and Lastname *</label>
                                            <input type="text" name="name_review" id="name_review" placeholder="" class="form-control">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Email *</label>
                                            <input type="email" name="email_review" id="email_review" class="form-control">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Rating </label>
                                            <div class="custom-select-form">
                                            <select name="rating_review" id="rating_review" class="wide">
                                                <option value="1">1 (lowest)</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5" selected>5 (medium)</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10 (highest)</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>Your Review</label>
                                            <textarea name="review_text" id="review_text" class="form-control" style="height:130px;"></textarea>
                                        </div>
                                        <div class="form-group col-md-12 add_top_20 add_bottom_30">
                                            <input type="submit" value="Submit" class="btn_1" id="submit-review">
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                    <!-- /col -->
                    
                    <aside class="col-lg-4" id="sidebar">
                       <div class="box_detail booking">
                            <div class="price">
                                <span><h3>Contact {{ $listing->companyname }}</h3></span>
                               
                            </div>

                         @guest
                         <a href="{{ url('/login') }}" class=" add_top_30 btn_1 full-width purchase">Message {{ $listing->companyname }}</a>
                         @else
                          <form action="{{ route('listings.contact.store', [$area, $listing]) }}" method="post">
                         <div class="form-group clearfix{{ $errors->has('message') ? ' has-error' : '' }}">
                            <textarea class="form-control add_bottom_15" name="message" id="message" placeholder="Message {{ $listing->companyname }} and they will reply you directly to your email!" style="height: 150px;"></textarea>

                             @if ($errors->has('message'))
                                    <span class="help-block">
                                        {{ $errors->first('message') }}
                                    </span>
                                @endif
                         </div>
                         <button type="submit" class="add_top_30 btn_1 full-width purchase">Send message</button>

                          {{ csrf_field() }}
                           </form>
                         @endguest  
                        </div>
                        <ul class="share-buttons">
                            <li><a class="fb-share" href="#0"><i class="social_facebook"></i> Share</a></li>
                            <li><a class="twitter-share" href="#0"><i class="social_twitter"></i> Tweet</a></li>
                            <li><a class="gplus-share" href="#0"><i class="social_googleplus"></i> Share</a></li>
                        </ul>
                    </aside>
                </div>
                <!-- /row -->
        </div>
        <!-- /container -->
        
    </main>
    <!--/main-->
   <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
})
    </script>
@endsection

@section('more-scripts')
    @php
        env('GOOGLE_MAPS_API_KEY') ? $mapsApiKey = '?key='.env('GOOGLE_MAPS_API_KEY') : $mapsApiKey = '';
    @endphp
    {{-- Load the maps js to show place on map--}}
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js{{$mapsApiKey}}"></script>
    <script src="/show-on-map.js"></script>
@endsection
