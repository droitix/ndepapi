@extends('layouts.listapp')

@section('content')

@include('includes.listingbanner')


<div id="wrapper">

  
    
    <div class="main-container">
        <div class="container">
            <div class="row">
                <!-- this (.mobile-filter-sidebar) part will be position fixed in mobile version -->
                <div class="col-md-3 page-sidebar mobile-filter-sidebar">
                    <aside>
                        <div class="sidebar-modern-inner">
                            <div class="block-title has-arrow sidebar-header">
                                <h5><a href="#">More Areas in {{ $area->name }}</a></h5>
                            </div>
                            <div class="block-content categories-list  list-filter ">
                                <ul class=" list-unstyled">
                                     @foreach ($area->children as $city)
                                    <li><a href="{{ route('user.area.store', $city) }}"><span class="title">{{ $city->name }}</span><span class="count"></span></a>
                                    </li>
                                    @endforeach
                                  
                                </ul>
                            </div>  <!--/.categories-list-->    
                    </aside>
                </div>
                <!--/.page-side-bar-->
                <div class="col-md-9 page-content col-thin-left">

                    <div class="category-list">
                        <div class="tab-content">
                            <div class="tab-pane  active " id="alladslist">
                                <div class="adds-wrapper row no-margin property-list">
                                    


                                 
    @if ($listings->count())
        @foreach ($listings as $listing)
            @include ('listings.partials._listing', compact('listing'))
        @endforeach


{{ $listings->links() }}
    @else
        <div class="container">
        
            <div class="col-md-8 ">
                <div class="panel panel-default" style="margin-top:40px;">
                   
                    <div class="panel-body">
                       
                                <div role="tabpanel" class="tab-pane section close-account" id="close-account">
                            <h1>There are currently no listings in this category</h1>
                            <span>Be first to post {{ $category->name }} here in {{ $area->name }}</span>
                            <div class="buttons">
                                 <a href="{{ route('listings.create', [$area]) }}"><button class="btn btn-primary">Add listing</button></a> 
                                
                            </div>
                        </div><!-- /.tab-pane -->
                                

                    </div>
                </div>
            </div>
      
    </div>
    @endif

                                    


                                </div>
                                <!--/.adds-wrapper-->
                            </div>
                          </div>



                        

                </div>
                <!--/.page-content-->

            </div>
        </div>
    </div>
    <!-- /.main-container -->


</div>
<!-- /.wrapper -->



<!-- Le javascript
================================================== -->

<!-- Placed at the end of the document so the pages load faster -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery/jquery-3.3.1.min.js">\x3C/script>')</script>

<script src="assets/js/vendors.min.js"></script>

<!-- include custom script for site  -->
<script src="assets/js/main.min.js"></script>




</body>
</html>

@endsection
