@extends('layouts.v2adminapp')


@section('content')

 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Register Doctor</li>
      </ol>

        <div class="box_general padding_bottom">
            <div class="header_box version_2">
                <h2><i class="fas fa-file"></i>Personal and Surgery info</h2>
            </div>
             <form action="{{ route('listings.store', [$area]) }}" method="post">
                                  <input type="hidden" name="image_names" id="image_names">
            <div class="row">
                <div class="col-md-6">

                    <div class="form-group{{ $errors->has('companyname') ? ' has-error' : '' }}">
                        <label>Title or Company Name</label>
                        <input type="text" name="companyname" id="companyname" class="form-control" placeholder="e.g Muzinda Lodge">

                            @if ($errors->has('companyname'))
                                <span class="help-block">
                                    {{ $errors->first('companyname') }}
                                </span>
                            @endif
                    </div>
                </div>
                <div class="col-md-6">
                    @include('listings.partials.forms._categories')
                </div>
            </div>
            <!-- /row-->
           <div class="row">
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label>Describe it in 3 separate words</label>
                         <input type="text" name="title" id="title" class="form-control" placeholder="seperate your words by commas">

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    {{ $errors->first('title') }}
                                </span>
                            @endif
                    </div>
                </div>
            </div>
            <!-- /row-->
            <!-- /row-->
            <div class="row">
                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}" style="margin-left: 19px;">
                            <label for="body" class="control-label">Describe your company or Organisation (Required)</label>
                            <textarea name="body" id="body" cols="50" rows="8" class="form-control"  ></textarea>

                            @if ($errors->has('body'))
                                <span class="help-block">
                                    {{ $errors->first('body') }}
                                </span>
                            @endif
                        </div>
            </div>
            <!-- /row-->
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('phone1') ? ' has-error' : '' }}">
                        <label>Phone ( Required )</label>
                         <input type="text" name="phone1" id="phone1" class="form-control" placeholder="e.g 775291423 without 0">

                            @if ($errors->has('phone1'))
                                <span class="help-block">
                                    {{ $errors->first('phone1') }}
                                </span>
                            @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                        <label>Web site (Optional)</label>
                        <input type="text" name="website" id="website" class="form-control" placeholder="e.g www.ndepapi.co.zw">

                            @if ($errors->has('website'))
                                <span class="help-block">
                                    {{ $errors->first('website') }}
                                </span>
                            @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('companyemail') ? ' has-error' : '' }}">
                        <label>Email ( Recommended )</label>
                        <input type="text" name="companyemail" id="companyemail" class="form-control" placeholder="e.g apple@gmail.com">

                            @if ($errors->has('companyemail'))
                                <span class="help-block">
                                    {{ $errors->first('companyemail') }}
                                </span>
                            @endif
                    </div>
                </div>
            </div>
            <!-- /row-->
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
                        <label>Facebook link (Optional)</label>
                        <input type="text" name="facebook" id="facebook" class="form-control" placeholder="e.g https://facebook.com/paulkumbweya">

                            @if ($errors->has('facebook'))
                                <span class="help-block">
                                    {{ $errors->first('facebook') }}
                                </span>
                            @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }}">
                        <label>Twitter link (Optional)</label>
                        <input type="text" name="twitter" id="twitter" class="form-control" placeholder="e.g https://twitter.com/paulkumz">

                            @if ($errors->has('twitter'))
                                <span class="help-block">
                                    {{ $errors->first('twitter') }}
                                </span>
                            @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('instagram') ? ' has-error' : '' }}">
                        <label>Instagram (Optional)</label>
                        <input type="text" name="instagram" id="instagram" class="form-control" placeholder="e.g https://instagram.com/paulkumz">

                            @if ($errors->has('instagram'))
                                <span class="help-block">
                                    {{ $errors->first('instagram') }}
                                </span>
                            @endif
                    </div>
                </div>
            </div>
            <!-- /row-->
            <div class="row">
                <div class="col-md-12">
                   
                        <div class="row form-group add-image">
                                        <label class="col-sm-3 label-title">Listing Photos <span></span> </label>
                                        <div class="col-sm-9">
                                            <h5><i class="fa fa-upload" aria-hidden="true"></i>Select Files to Upload / Drag and Drop Files <span>You can add multiple images.</span></h5>
                                            <div class="upload-section">

                                                <div class="form-group" style="margin-top:10px">
                                                    <label for="listing_image" style="text-align:center;"><font color="brown">**Images are required and a minimum of 300 x 300pixels is expected</font></label>
                                                
                                                    <div class="form-group">
                                                        <div class="file-loading">
                                                            <input id="listing_image" type="file" name="listing_image[]" multiple class="file" data-overwrite-initial="false">
                                                        </div>
                                                    </div>
                                                </div> 

                                            </div>  
                                        </div>
                                   
                    </div>
                </div>
            </div>
            <!-- /row-->
        </div>
        <!-- /box_general-->
        <div class="box_general padding_bottom">
            <div class="header_box version_2">
                <h2><i class="fa fa-clock-o"></i>Add your listing opening and closing times(optional)</h2>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="fix_spacing">Monday</label>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                        <select name="monopening" id="monopening">
                            <option value="">Opening Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                        <select name="monclosing" id="monclosing">
                             <option value="">Closing Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-->
            <div class="row">
                <div class="col-md-2">
                    <label class="fix_spacing">Tuesday</label>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                        <select name="tuesopening" id="tuesopening">
                             <option value="">Opening Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                      <select name="tuesclosing" id="tuesclosing">
                             <option value="">Closing Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-->
            <div class="row">
                <div class="col-md-2">
                    <label class="fix_spacing">Wednesday</label>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                      <select name="wedopening" id="wedopening">
                             <option value="">Opening Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                       <select name="wedclosing" id="wedclosing">
                            <option value="">Closing Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-->
            <div class="row">
                <div class="col-md-2">
                    <label class="fix_spacing">Thursday</label>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                       <select name="thursopening" id="thursopening">
                             <option value="">Opening Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                       <select name="thursclosing" id="thursclosing">
                            <option value="">Closing Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-->
                <div class="row">
                <div class="col-md-2">
                    <label class="fix_spacing">Friday</label>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                      <select name="fridayopening" id="fridayopening">
                            <option value="">Opening Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                        <select name="fridayclosing" id="fridayclosing">
                             <option value="">Closing Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-->
                <div class="row">
                <div class="col-md-2">
                    <label class="fix_spacing">SURTUDAY</label>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                       <select name="surtopening" id="surtopening">
                             <option value="">Opening Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                        <select name="surtclosing" id="surtclosing">
                             <option value="">Closing Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-->
                <div class="row">
                <div class="col-md-2">
                    <label class="fix_spacing">SUNDAY</label>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                        <select name="sunopening" id="sunopening">
                             <option value="">Opening Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="styled-select">
                       <select name="sunclosing" id="sunclosing">
                             <option value="">Closing Time</option>
                            <option value="Closed">Closed</option>
                            <option value="1 AM">1 AM</option>
                            <option value="2 AM">2 AM</option>
                            <option value="3 AM">3 AM</option>
                            <option value="4 AM">4 AM</option>
                            <option value="5 AM">5 AM</option>
                            <option value="6 AM">6 AM</option>
                            <option value="7 AM">7 AM</option>
                            <option value="8 AM">8 AM</option>
                            <option value="9 AM">9 AM</option>
                            <option value="10 AM">10 AM</option>
                            <option value="11 AM">11 AM</option>
                            <option value="12 AM">12 AM</option>  
                            <option value="1 PM">1 PM</option>
                            <option value="2 PM">2 PM</option>
                            <option value="3 PM">3 PM</option>
                            <option value="4 PM">4 PM</option>
                            <option value="5 PM">5 PM</option>
                            <option value="6 PM">6 PM</option>
                            <option value="7 PM">7 PM</option>
                            <option value="8 PM">8 PM</option>
                            <option value="9 PM">9 PM</option>
                            <option value="10 PM">10 PM</option>
                            <option value="11 PM">11 PM</option>
                            <option value="12 PM">12 PM</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-->
        </div>
        <!-- /box_general-->
        <div class="box_general padding_bottom">
            <div class="header_box version_2">
                <h2><i class="fas fa-map-marker-alt"></i>Location</h2>
            </div>
            <div class="row">
                <div class="col-md-6">
                   @include('listings.partials.forms._areas')
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <label>Address ( Required )</label>
                        <input type="text" name="address" id="address" class="form-control" placeholder="e.g 34 Sterling Road, Eastlea ,Harare">

                            @if ($errors->has('address'))
                                <span class="help-block">
                                    {{ $errors->first('address') }}
                                </span>
                            @endif
                    </div>
                </div>
            </div>
            <!-- /row-->
           
            <!-- /row-->
        </div>
        <!-- /box_general-->
        
        
            
      
        
        <div class="box_general padding_bottom">
      
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Post listing</button>
                        </div>
        </div>
        <!-- /box_general-->
     
                        {{ csrf_field() }}
                        </form><!-- form -->    
      </div>
      <!-- /.container-fluid-->
    </div>
    <!-- /.container-wrapper-->
 <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>
    
    <script type="text/javascript">

        let userId = {{ \Auth::user()->id }}
        console.log(userId);
        let listingImages = $("#listing_image");
            
        $('.remove-image').on('click', function(e){
            
            $('#removed_images').val(function(i, val) {
                return val + e.target.dataset.imagePath + '|';
            });
            $(this).parents('.item-image.item-thumbnail').remove();
            listingImages.fileinput('refresh');
        });
            
        listingImages.fileinput({
            theme: 'fa',
            uploadUrl: `/upload-image/${userId}`,
            uploadAsync: false,
            showUpload: false, // hide upload button
            showRemove: false, // hide remove button
            validateInitialCount: true,
            overwriteInitial: false,
            // minFileCount: 0,
            maxFileCount: 1,
            initialPreviewAsData: true,
            uploadExtraData: function() {
                return {
                    _token: $("input[name='_token']").val(),
                };
            },
        }).on("filebatchselected", function(event, files) {
            // console.log(event, files);
            listingImages.fileinput("upload");
        });

        // CATCH RESPONSE
        listingImages.on('filebatchuploaderror', function(event, data, previewId, index) {
            var form = data.form, files = data.files, extra = data.extra, 
            response = data.response, reader = data.reader;
        });

        listingImages.on('filebatchuploadsuccess', function(event, data, previewId, index) {
            if(data.jqXHR.responseJSON){
                response = data.jqXHR.responseJSON;
                if(data.jqXHR.responseJSON.success == true){
                    $("#image_names").val(function(i, val) {
                        return val += data.jqXHR.responseJSON.imagePaths + '|';
                    });
                }
            }
        });

    </script>

@endsection