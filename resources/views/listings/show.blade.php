@extends('layouts.appclass')

@section('content')
  
    
   
    
    <main>    
             

                        
            <div class="section slider" style="margin-left: 3vw; margin-top: 1vw;">


             <div class="detail_title_1">
                               
                                <h1>{{ $listing->companyname }}</h1>
                                <span style="margin-top: 2vw;"><a href="" class="address" >{{ $listing->address }}</a></span>
              </div>                           
                <div class="row">
                    <!-- carousel -->

                    <div class="col-md-7">
                        <div id="product-carousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators" >
                                
                                @if($listing->images != '')
                                    @foreach($listing->images as $key => $image)
                                        
                                            <li data-target="#product-carousel" data-slide-to="{{$key}}" @if($key == 0) class="active" @endif>
                                                <img  src="{{'/storage/'.$image}}" alt="Carousel Thumb" class="image-responsive">
                                            </li>  
                                        
                                    @endforeach

                           
                                @endif

                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">

                                @if(count($listing->images) > 0)
                                    @foreach($listing->images as $key => $image)
                                        <?php//echo $key . $image; ?>
                                        <!-- item -->
                                        <div class="item @php if($key == 0) echo 'active'; @endphp">
                                            <div class="carousel-image" style="max-height: 400px; min-height: 400px; ">
                                                <!-- image-wrapper -->
                                                <img src="{{'/storage/'.$image}}" alt="Featured Image" class="img-resp">
                                            </div>
                                        </div><!-- item -->
                                        
                                    @endforeach

                                    @else
                                    <div class="carousel-image">
                                    <img src="{{'/images/featured/2.jpg'}}" alt="Featured Image" class="img-resp">
                                    </div>
                                    
                                @endif

                            </div><!-- carousel-inner -->

                            <!-- Controls -->
                            <a class="left carousel-control" href="#product-carousel" role="button" data-slide="prev">
                                <i class="fa fa-chevron-left"></i>
                            </a>
                            <a class="right carousel-control" href="#product-carousel" role="button" data-slide="next">
                                <i class="fa fa-chevron-right"></i>
                            </a><!-- Controls -->
                        </div>
                    </div><!-- Controls --> 

                    <!-- slider-text -->
                    <div class="col-md-5">
                        <div class="slider-text">

                            <h3>{{ $listing->companyname }}<span class="pull-right"><font color="purple"> ({{ $listing->views() }}) Views </font></span></h3>
                         <span style="line-height:2.2em;"><i class="fas fa-map-marked-alt" style="color:green;"></i>{{ $listing->address }}</span>
                            <p>
                            <span>Listed Since:<a href="#" class="time"><time>{{ $listing->created_at->diffForHumans() }}</time></a></span>

                                <button id="show-address" class="btn btn-success">
                                    <i class="fa fa-map-marker-alt" data-toggle="modal" 
                                    data-target="#mapModal" data-address="{{$listing->address}}"
                                    data-place-name="{{$listing->companyname}}"
                                    data-category="{{$listing->category->id}}"
                                    data-area="{{$area->slug}}">View on Map</i> 
                                </button>  

                            </p>

                           
                            <!-- short-info -->
                            <div class="short-info">
                                <h4>Why us?</h4>
                                <p>{{ $listing->body }}</p>
                                
                            </div><!-- short-info -->
                            
                            <!-- contact-with -->
                            <div class="contact-with">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="{{ $listing->companyname }}"><i class="far fa-paper-plane"></i>Send Enquiry</button>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       <div class="panel panel-default">
                <div class="panel-heading">
                    Contact {{ $listing->user->name }}
                </div>
                <div class="panel-body">
                    @if (Auth::guest())
                        <p><a href="/register">Sign up</a> for an account or <a href="/login">sign in</a> to contact listing owners.</p>
                    @else
                        
                        <form action="{{ route('listings.contact.store', [$area, $listing]) }}" method="post">
                            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                <label for="message" class="control-label">Message</label>
                                <textarea name="message" id="message" cols="30" rows="5" class="form-control"></textarea>

                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        {{ $errors->first('message') }}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default">Send</button>
                                <span class="help-block">
                                    This will email {{ $listing->user->name }} and they'll be able to reply directly to you by email.
                                </span>
                            </div>

                            {{ csrf_field() }}
                        </form>
                        
                    @endif
                </div>
            </div>
    </div>
  </div>
</div>
                                
                            </div><!-- contact-with -->
                            
                           <span><i class="fas fa-check" style="color:green;"></i>This is verified information</span>  
                           <br>
                           <br>
                          

                           <!-- Load Facebook SDK for JavaScript -->
   <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your share button code -->
  <div class="fb-share-button" 
    data-href="{{ route('listings.show', [$area, $listing]) }}" 
    data-layout="button_count">
  </div>


                        </div>
                    </div><!-- slider-text -->              
                </div>              
            </div><!-- slider -->

           

                    <nav class="secondary_nav sticky_horizontal_2">
            <div class="container">
                <ul class="clearfix">
                    <li><a href="#description" class="active">Description</a></li>
                    <li><a href="#reviews">Reviews</a></li>
                    <li><a href="#sidebar">Booking</a></li>
                </ul>
            </div>
        </nav>

        <div class="container margin_60_35">
                <div class="row">
                    <div class="col-lg-8">
                        <section id="description">
                            <div class="detail_title_1">
                                <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
                                <h1>Hotel Mariott</h1>
                                <a class="address" href="https://www.google.com/maps/dir//Assistance+%E2%80%93+H%C3%B4pitaux+De+Paris,+3+Avenue+Victoria,+75004+Paris,+Francia/@48.8606548,2.3348734,14z/data=!4m15!1m6!3m5!1s0x47e66e1de36f4147:0xb6615b4092e0351f!2sAssistance+Publique+-+H%C3%B4pitaux+de+Paris+(AP-HP)+-+Si%C3%A8ge!8m2!3d48.8568376!4d2.3504305!4m7!1m0!1m5!1m1!1s0x47e67031f8c20147:0xa6a9af76b1e2d899!2m2!1d2.3504327!2d48.8568361">438 Rush Green Road, Romford</a>
                            </div>
                            <p>Per consequat adolescens ex, cu nibh commune <strong>temporibus vim</strong>, ad sumo viris eloquentiam sed. Mea appareat omittantur eloquentiam ad, nam ei quas oportere democritum. Prima causae admodum id est, ei timeam inimicus sed. Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</p>
                            <p>Cum et probo menandri. Officiis consulatu pro et, ne sea sale invidunt, sed ut sint <strong>blandit</strong> efficiendi. Atomorum explicari eu qui, est enim quaerendum te. Quo harum viris id. Per ne quando dolore evertitur, pro ad cibo commune.</p>
                            <h5 class="add_bottom_15">Amenities</h5>
                            <div class="row add_bottom_30">
                                <div class="col-lg-6">
                                    <ul class="bullets">
                                        <li>Dolorem mediocritatem</li>
                                        <li>Mea appareat</li>
                                        <li>Prima causae</li>
                                        <li>Singulis indoctum</li>
                                    </ul>
                                </div>
                                <div class="col-lg-6">
                                    <ul class="bullets">
                                        <li>Timeam inimicus</li>
                                        <li>Oportere democritum</li>
                                        <li>Cetero inermis</li>
                                        <li>Pertinacia eum</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /row -->                       
                            <hr>
                            <div class="room_type first">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/gallery/hotel_list_1.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        <h4>Single Room</h4>
                                        <p>Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</p>
                                        <ul class="hotel_facilities">
                                            <li><img src="img/hotel_facilites_icon_2.svg" alt="">Single Bed</li>
                                            <li><img src="img/hotel_facilites_icon_4.svg" alt="">Free Wifi</li>
                                            <li><img src="img/hotel_facilites_icon_5.svg" alt="">Shower</li>
                                            <li><img src="img/hotel_facilites_icon_7.svg" alt="">Air Condition</li>
                                            <li><img src="img/hotel_facilites_icon_8.svg" alt="">Hairdryer</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /row -->
                            </div>
                            <!-- /rom_type -->
                            <div class="room_type alternate">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/gallery/hotel_list_2.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        <h4>Double Room</h4>
                                        <p>Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</p>
                                        <ul class="hotel_facilities">
                                            <li><img src="img/hotel_facilites_icon_3.svg" alt="">Double Bed</li>
                                            <li><img src="img/hotel_facilites_icon_4.svg" alt="">Free Wifi</li>
                                            <li><img src="img/hotel_facilites_icon_6.svg" alt="">Bathtub</li>
                                            <li><img src="img/hotel_facilites_icon_7.svg" alt="">Air Condition</li>
                                            <li><img src="img/hotel_facilites_icon_8.svg" alt="">Hairdryer</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /row -->
                            </div>
                            <!-- /rom_type -->
                            <div class="room_type last">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/gallery/hotel_list_3.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        <h4>Suite Room</h4>
                                        <p>Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</p>
                                        <ul class="hotel_facilities">
                                            <li><img src="img/hotel_facilites_icon_3.svg" alt="">King size Bed</li>
                                            <li><img src="img/hotel_facilites_icon_4.svg" alt="">Free Wifi</li>
                                            <li><img src="img/hotel_facilites_icon_6.svg" alt="">Bathtub</li>
                                            <li><img src="img/hotel_facilites_icon_7.svg" alt="">Air Condition</li>
                                            <li><img src="img/hotel_facilites_icon_9.svg" alt="">Swimming pool</li>
                                            <li><img src="img/hotel_facilites_icon_3.svg" alt="">Hairdryer</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /row -->
                            </div>
                            <!-- /rom_type -->
                            <hr>
                            <h3>Prices</h3>
                            <table class="table table-striped add_bottom_45">
                                <tbody>
                                    <tr>
                                        <td>Low (from 23/03 to 31/05)</td>
                                        <td>140$</td>
                                    </tr>
                                    <tr>
                                        <td>Middle (from 23/03 to 31/05)</td>
                                        <td>180$</td>
                                    </tr>
                                    <tr>
                                        <td>High (from 23/03 to 31/05)</td>
                                        <td>200$</td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>
                            <h3>Location</h3>
                            <div id="map" class="map map_single add_bottom_45"></div>
                            <!-- End Map -->
                        </section>
                        <!-- /section -->
                    
                        <section id="reviews">
                            <h2>Reviews</h2>
                            <div class="reviews-container add_bottom_30">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div id="review_summary">
                                            <strong>8.5</strong>
                                            <em>Superb</em>
                                            <small>Based on 4 reviews</small>
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-lg-10 col-9">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-3"><small><strong>5 stars</strong></small></div>
                                        </div>
                                        <!-- /row -->
                                        <div class="row">
                                            <div class="col-lg-10 col-9">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-3"><small><strong>4 stars</strong></small></div>
                                        </div>
                                        <!-- /row -->
                                        <div class="row">
                                            <div class="col-lg-10 col-9">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-3"><small><strong>3 stars</strong></small></div>
                                        </div>
                                        <!-- /row -->
                                        <div class="row">
                                            <div class="col-lg-10 col-9">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-3"><small><strong>2 stars</strong></small></div>
                                        </div>
                                        <!-- /row -->
                                        <div class="row">
                                            <div class="col-lg-10 col-9">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" style="width: 0" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-3"><small><strong>1 stars</strong></small></div>
                                        </div>
                                        <!-- /row -->
                                    </div>
                                </div>
                                <!-- /row -->
                            </div>

                            <div class="reviews-container">

                                <div class="review-box clearfix">
                                    <figure class="rev-thumb"><img src="img/avatar1.jpg" alt="">
                                    </figure>
                                    <div class="rev-content">
                                        <div class="rating">
                                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
                                        </div>
                                        <div class="rev-info">
                                            Admin – April 03, 2016:
                                        </div>
                                        <div class="rev-text">
                                            <p>
                                                Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /review-box -->
                                <div class="review-box clearfix">
                                    <figure class="rev-thumb"><img src="img/avatar2.jpg" alt="">
                                    </figure>
                                    <div class="rev-content">
                                        <div class="rating">
                                            <i class="icon-star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
                                        </div>
                                        <div class="rev-info">
                                            Ahsan – April 01, 2016:
                                        </div>
                                        <div class="rev-text">
                                            <p>
                                                Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /review-box -->
                                <div class="review-box clearfix">
                                    <figure class="rev-thumb"><img src="img/avatar3.jpg" alt="">
                                    </figure>
                                    <div class="rev-content">
                                        <div class="rating">
                                            <i class="icon-star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
                                        </div>
                                        <div class="rev-info">
                                            Sara – March 31, 2016:
                                        </div>
                                        <div class="rev-text">
                                            <p>
                                                Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /review-box -->
                            </div>
                            <!-- /review-container -->
                        </section>
                        <!-- /section -->
                        <hr>

                            <div class="add-review">
                                <h5>Leave a Review</h5>
                                <form>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Name and Lastname *</label>
                                            <input type="text" name="name_review" id="name_review" placeholder="" class="form-control">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Email *</label>
                                            <input type="email" name="email_review" id="email_review" class="form-control">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Rating </label>
                                            <div class="custom-select-form">
                                            <select name="rating_review" id="rating_review" class="wide">
                                                <option value="1">1 (lowest)</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5" selected>5 (medium)</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10 (highest)</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>Your Review</label>
                                            <textarea name="review_text" id="review_text" class="form-control" style="height:130px;"></textarea>
                                        </div>
                                        <div class="form-group col-md-12 add_top_20 add_bottom_30">
                                            <input type="submit" value="Submit" class="btn_1" id="submit-review">
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                    <!-- /col -->
                    
                    <aside class="col-lg-4" id="sidebar">
                        <div class="box_detail booking">
                            <div class="price">
                                <span>45$ <small>person</small></span>
                                <div class="score"><span>Good<em>350 Reviews</em></span><strong>7.0</strong></div>
                            </div>

                            <div class="form-group" id="input-dates">
                                <input class="form-control" type="text" name="dates" placeholder="When..">
                                <i class="icon_calendar"></i>
                            </div>

                            <div class="panel-dropdown">
                                <a href="#">Guests <span class="qtyTotal">1</span></a>
                                <div class="panel-dropdown-content right">
                                    <div class="qtyButtons">
                                        <label>Adults</label>
                                        <input type="text" name="qtyInput" value="1">
                                    </div>
                                    <div class="qtyButtons">
                                        <label>Childrens</label>
                                        <input type="text" name="qtyInput" value="0">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <div class="custom-select-form">
                                    <select class="wide">
                                        <option>Room Type</option>  
                                        <option>Single Room</option>
                                        <option>Double Room</option>
                                        <option>Suite Room</option>
                                    </select>
                                </div>
                            </div>
                            <a href="checkout.html" class=" add_top_30 btn_1 full-width purchase">Purchase</a>
                            <a href="wishlist.html" class="btn_1 full-width outline wishlist"><i class="icon_heart"></i> Add to wishlist</a>
                            <div class="text-center"><small>No money charged in this step</small></div>
                        </div>
                        <ul class="share-buttons">
                            <li><a class="fb-share" href="#0"><i class="social_facebook"></i> Share</a></li>
                            <li><a class="twitter-share" href="#0"><i class="social_twitter"></i> Tweet</a></li>
                            <li><a class="gplus-share" href="#0"><i class="social_googleplus"></i> Share</a></li>
                        </ul>
                    </aside>
                </div>
                <!-- /row -->
        </div>
        <!-- /container -->
        
    </main>
    <!--/main-->

    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
})
    </script>
@endsection

@section('more-scripts')
    @php
        env('GOOGLE_MAPS_API_KEY') ? $mapsApiKey = '?key='.env('GOOGLE_MAPS_API_KEY') : $mapsApiKey = '';
    @endphp
    {{-- Load the maps js to show place on map--}}
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js{{$mapsApiKey}}"></script>
    <script src="/show-on-map.js"></script>
@endsection
