@extends('layouts.appupload')

@section('edit-page-styles-scripts')

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>


@endsection

@section('content')
    
    <div class="tr-breadcrumb bg-image section-before">
        <div class="container">
            <div class="breadcrumb-info text-center">
                <div class="page-title">
                    <h1>......Continue Editing</h1>
                </div>      
                   
            </div>
        </div><!-- /.container -->
    </div><!-- /.tr-breadcrumb -->

    <div class="tr-post-job page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="short-info code-edit-small">
                        <div class="section">
                            <span class="tr-title">..Continue editing listing
                    @if ($listing->live())
                        <span class="pull-right"><a href="{{ route('listings.show', [$area, $listing]) }}">Go to listing</a></span>
                    @endif</span>
                            <form action="{{ route('listings.update', [$area, $listing]) }}" method="post">
                    <div class="row">
                        @include('listings.partials.forms._areas')
                         @include('listings.partials.forms._categories')
                    </div>
                    
                    <div class="row">
                        <div class="form-group{{ $errors->has('companyname') ? ' has-error' : '' }} col-md-8">
                            <label for="companyname" class="control-label">Company or Organisation name</label>
                            <input type="text" name="companyname" id="companyname" class="form-control" placeholder="e.g Muzinda Lodge" value="{{ $listing->companyname }}">

                            @if ($errors->has('companyname'))
                                <span class="help-block">
                                    {{ $errors->first('companyname') }}
                                </span>
                            @endif
                        </div>
                        </div>

                         <div class="row">
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }} col-md-6">
                            <label for="title" class="control-label">Brief title</label>
                            <input type="text" name="title" id="title" class="form-control" placeholder="e.g Digital marketing company" value="{{ $listing->title }}">

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    {{ $errors->first('title') }}
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} col-md-5">
                            <label for="address" class="control-label">Address or Location</label>
                            <input type="text" name="address" id="address" class="form-control" placeholder="e.g 34 Sterling Road, Eastlea" value="{{ $listing->address }}">

                            @if ($errors->has('address'))
                                <span class="help-block">
                                    {{ $errors->first('address') }}
                                </span>
                            @endif
                        </div>
                        </div>
 
                         <div class="row">
                        <div class="form-group{{ $errors->has('phone1') ? ' has-error' : '' }} col-md-4">
                            <label for="phone1" class="control-label">phone 1 (at least one phone)</label>
                            <input type="text" name="phone1" id="phone1" class="form-control" placeholder="e.g 775291423" value="{{ $listing->phone1 }}">

                            @if ($errors->has('phone1'))
                                <span class="help-block">
                                    {{ $errors->first('phone1') }}
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('phone2') ? ' has-error' : '' }} col-md-4">
                            <label for="phone1" class="control-label">phone 2</label>
                            <input type="text" name="phone2" id="phone2" class="form-control" value="{{ $listing->phone2 }}" >

                            @if ($errors->has('phone2'))
                                <span class="help-block">
                                    {{ $errors->first('phone2') }}
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('phone3') ? ' has-error' : '' }} col-md-4">
                            <label for="phone3" class="control-label">phone 3</label>
                            <input type="text" name="phone3" id="phone3" class="form-control" value="{{ $listing->phone3 }}">

                            @if ($errors->has('phone3'))
                                <span class="help-block">
                                    {{ $errors->first('phone3') }}
                                </span>
                            @endif
                        </div>
                        </div>

                         <div class="row">
                        <div class="form-group{{ $errors->has('companyemail') ? ' has-error' : '' }} col-md-6">
                            <label for="companyemail" class="control-label">Email</label>
                            <input type="text" name="companyemail" id="companyemail" class="form-control" placeholder="e.g apple@gmail.com" value="{{ $listing->companyemail }}">

                            @if ($errors->has('companyemail'))
                                <span class="help-block">
                                    {{ $errors->first('companyemail') }}
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }} col-md-5">
                            <label for="website" class="control-label">Website</label>
                            <input type="text" name="website" id="website" class="form-control" placeholder="e.g https://ndepapi.com" value="{{ $listing->website }}">

                            @if ($errors->has('website'))
                                <span class="help-block">
                                    {{ $errors->first('website') }}
                                </span>
                            @endif
                        </div>
                        </div>
                             
                        <div class="row">
                        <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }} col-md-4">
                            <label for="facebook" class="control-label">Facebook</label>
                            <input type="text" name="facebook" id="facebook" class="form-control" placeholder="e.g https://facebook.com/paulkumbweya" value="{{ $listing->facebook }}">

                            @if ($errors->has('facebook'))
                                <span class="help-block">
                                    {{ $errors->first('facebook') }}
                                </span>
                            @endif
                        </div>

                       <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }} col-md-4">
                            <label for="twitter" class="control-label">Twitter</label>
                            <input type="text" name="twitter" id="twitter" class="form-control" placeholder="e.g https://twitter.com/paulkumz" value="{{ $listing->twitter }}">

                            @if ($errors->has('twitter'))
                                <span class="help-block">
                                    {{ $errors->first('twitter') }}
                                </span>
                            @endif
                        </div>


                        <div class="form-group{{ $errors->has('instagram') ? ' has-error' : '' }} col-md-4">
                            <label for="instagram" class="control-label">Instagram</label>
                            <input type="text" name="instagram" id="instagram" class="form-control" placeholder="e.g https://instagram.com/paulkumz" value="{{ $listing->instagram }}">

                            @if ($errors->has('instagram'))
                                <span class="help-block">
                                    {{ $errors->first('instagram') }}
                                </span>
                            @endif
                        </div>

                        </div>
     
                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <label for="body" class="control-label">Full company or Organisation description</label>
                            <textarea name="body" id="body" cols="30" rows="8" class="form-control">{{ $listing->body }}</textarea>

                            @if ($errors->has('body'))
                                <span class="help-block">
                                    {{ $errors->first('body') }}
                                </span>
                            @endif
                        </div>
                        <div class="form-group clearfix">
                            <button type="submit" class="btn btn-primary">Save</button>

                            @if (!$listing->live())
                                <button type="submit" name="payment" value="true" class="btn btn-primary pull-right">Finalize</button>
                            @endif
                        </div>

                        @if ($listing->live())
                            <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                        @endif

                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                    </form>                                                
                        </div>
                    </div>
                   <div class="container col-md-4">
                @if($listing->images != '')
                    @foreach($listing->images as $key => $image)
                        
                            <div data-target="#product-carousel" data-slide-to="{{$key}}" @if($key == 0) class="active" @endif style="width:60%;height:50%;">
                                
                                <img src="{{'/storage/'.$image}}" alt="Carousel Thumb" class="img-responsive">

                                <button class="remove-image  btn-danger" data-button-function="remove" data-image-id={{$image}}>Remove Image</button>
                            
                            </div>  
                        
                    @endforeach
                @endif

            </div>
                   
                </div>
                <div class="col-sm-3">
                    <div class="section quick-rules">
                        <span class="tr-title">Quick Rules</span>
                        <p>Posting an ad on <a href="#">Seeker.com</a> is free! However, all post must follow our rules:</p>
                        <ul class="tr-list">
                            <li>Make sure you post in the correct category.</li>
                            <li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
                            <li>Do not upload pictures with watermarks.</li>
                            <li>Do not post ads containing multiple items unless it's a package deal.</li>
                            <li>Do not put your email or phone numbers in the title or description.</li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.tr-post-job -->


     <script>
        let removeImagesBtns = document.querySelectorAll('.remove-image');
        let imageNames = document.querySelector('#image_names');

        removeImagesBtns.forEach(function(item){
            item.addEventListener('click', removeImage, false);
        });

        function removeImage(e){
            if(e.target.dataset.buttonFunction === 'remove'){
                e.target.textContent = 'Add Image';
                e.target.dataset.buttonFunction = 'add';
                imageNames.value = (imageNames.value).replace(e.target.dataset.imageId, '');
            }else{
                e.target.textContent = 'Remove Image';
                e.target.dataset.buttonFunction = 'remove';
                imageNames.value += e.target.dataset.imageId + '|';
            }
        }
    </script> 

@endsection
