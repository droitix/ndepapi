
                <ul>
                    <li>
                        @if(count($listing->images) > 0)
                        <figure><a href="{{ route('listings.show', [$area, $listing]) }}">
                            <img src="{{'/storage/'.$listing->images[0]}}" alt=""></a>
                        </figure>
                        @else
                        <figure><img src="/svg/hotel4.jpg" alt=""></figure>
                        @endif
                        <h4><a href="{{ route('listings.show', [$area, $listing]) }}"> {{ $listing->companyname }}</a> <i class="pending">{{ $listing->category->name }}</i></h4>
                        <ul class="booking_list">
                            <li><strong>Listed on:</strong>{{ $listing->created_at }}</li>
                            <li><strong>Last edited</strong>{{ $listing->updated_at }}</li>
                            <li><strong>Location</strong>{{ $listing->address }} , Zimbabwe</li>
            s               
                        </ul>
                        
                        <ul class="buttons">
                          <li><a href="{{ route('listings.show', [$area, $listing]) }}" class="btn_1 gray approve"><i class="fas fa-eye"></i> Visit</a></li>
                            <li><a href="#" class="btn_1 gray delete" onclick="event.preventDefault(); document.getElementById('listings-favourites-destroy-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Remove favourite</a></li>
                        </ul>
                    </li>
                   
                </ul>
            
    
        <form action="{{ route('listings.favourites.destroy', [$area, $listing]) }}" method="post" id="listings-favourites-destroy-{{ $listing->id }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>