
                        <div class="item-list">

                                        <div class="row">

                                           @if(count($listing->images) > 0)
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fas fa-camera"></i> <?php echo count($listing->images) ?> </span> <a href="{{ route('listings.show', [$area, $listing]) }}"><img
                                                        class="thumbnail no-margin" src="{{'/storage/'.$listing->images[0]}}"
                                                        alt="img"></a>
                                                </div>
                                            </div>
                                            <!--/.photobox-->
                                            @else
                                             <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fas fa-camera"></i> 2 </span> <a href="{{ route('listings.show', [$area, $listing]) }}"><img
                                                        class="thumbnail no-margin" src="/images/55.jpg"
                                                        alt="img"></a>
                                                </div>
                                            </div>
                                            <!--/.photobox-->
                                            @endif
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="{{ route('listings.show', [$area, $listing]) }}">
                                                        {{ $listing->companyname }}</a></h5>
                                                    <span class="info-row"> <span class="item-location">{{ $listing->address }} |  <a><i
                                                            class="fa fa-map-marker-alt"></i> View on Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title "><?php echo rand(1, 9); ?></span>
                                                               <a href="">Reviews</a></span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <a href="{{ route('listings.share.index', [$area, $listing]) }}"><span class="title prop-area"><i class="fa fa-share" aria-hidden="true"></i>
</span>
                                                                <span class="text">Share</span></a>
                                                            </div>
                                                                                          

                             <div class="clearfix prop-info-block">
                                  <span class="title prop-room">

                               @if (Auth::check())
                                 @if (!$listing->favouritedBy(Auth::user()))
                      

                                
                                    <a href="#" onclick="event.preventDefault(); document.getElementById('listings-favourite-form').submit();">
                                        <i class="fa fa-heart" aria-hidden="true"></i>

                                    </span>    

                                         <span class="text">Save listing</span>

                                    </a>

                                    <form action="{{ route('listings.favourites.store', [$area, $listing]) }}" method="post" id="listings-favourite-form" class="hidden">
                                        {{ csrf_field() }}
                                    </form>
                                    @else
                                    <span class="title prop-area">
                                       <i class="fa fa-heart" aria-hidden="true" style="color:red;"></i>
                                       </span>
                                    <span class="text">Saved</span>
                                
                                 @endif
                                 @else
                                  <a href="{{ url('/login') }}">
                                 <span class="title prop-area">
                                    
                                    <i class="fa fa-heart" aria-hidden="true" style="color:brown;"></i>
                                       </span>
                                    <span class="text">Save listing</span>
                                    </a>
                             @endif
                         
                     </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                               

                                                <h3 class="item-price "> <strong>{{ $listing->area->name  }}</strong></h3>
                                                <div style="clear: both"></div>

                                                


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->