
                    <hr>
                        <div class="col-sm-12">
                            <div class="job-item ">

                                <div class="job-info">

                                    <div class="clearfix">
                                      
                                   @if(count($listing->images) > 0)
                                        <div class="company-logo">
                                            <a href="{{ route('listings.show', [$area, $listing]) }}">
                                            <img src="{{'/storage/'.$listing->images[0]}}" style="border-radius: 2px;" height="70" width="120" alt="images" >
                                        </a>
                                        </div>
                                      @else
                                        <div class="company-logo">
                                            <img src="/images/1.jpg" style="border-radius: 2px;" height="70" width="120" alt="images" class="img-responsive">
                                        </div>
                                  @endif
                                        <div class="container">
                                            
                                        <span class="tr-title col-sm-4" >
                                            <a href="{{ route('listings.show', [$area, $listing]) }}">{{ $listing->companyname }}</a>
                                        </span>

                                         
                                        </div>
                                        
                                    </div>
                                    <ul class="tr-list job-meta">
                                        <li><span><i class="fa fa-street-view" style="color:brown;" aria-hidden="true"></i></span> <font color="#81E979">{{ $listing->address }}, {{ $listing->area->parent->name }}</font></li>
                                        
                                        <li><span><a href="{{ route('listings.edit', [$area, $listing]) }}" class="btn btn-primary">Edit</a></span></li>
                                        <li><span><a href="#" class="btn btn-primary" style="color:red;">Delete</a></span></li>
                                         <li><span><a href="#" class="btn btn-primary" style="color:yellow;"><time>Listed:{{ $listing->created_at->diffForHumans() }}</time></a></span></li>
                                         <li><span><a href="#" class="btn btn-primary" style="color:yellow;">Last updated <time>{{ $listing->updated_at->diffForHumans() }}</time></a></span></li>
                                    </ul>
                                </div>
                            </div>
                             <hr>
                        </div>
                      
                                                   
