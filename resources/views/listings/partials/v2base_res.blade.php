   <div class="col-lg-3 col-sm-6">
                    <a href="{{ route('listings.show', [$area, $restaurant]) }}" class="grid_item small">
                        <figure>
                            <img src="{{'/storage/'.$restaurant->images[0]}}" alt="">
                            <div class="info">
                                <h3>{{ $restaurant->companyname }}</h3>
                                   <small style="background-color: #F3752B">in {{ $restaurant->area->name }}, {{ $restaurant->area->parent->name }}</small>
                            </div>
                        </figure>
                    </a>
                </div>