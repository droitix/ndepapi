@extends('layouts.appclass')



@section('content')
  
    
   
    
    <main>    
             

                 <section class="slider">     
            <div class="section slider" style="margin-left: 3vw; margin-top: 1vw;">


             <div class="detail_title_1">
                               
                                <h1>{{ $listing->companyname }}</h1>
                                <span style="margin-top: 2vw;"><a href="" class="address" >{{ $listing->address }}</a></span>
              </div>                           
                <div class="row">
                    <!-- carousel -->

                    <div class="col-md-7">
                        <div id="product-carousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators" >
                                
                                @if($listing->images != '')
                                    @foreach($listing->images as $key => $image)
                                        
                                            <li data-target="#product-carousel" data-slide-to="{{$key}}" @if($key == 0) class="active" @endif>
                                                <img  src="{{'/storage/'.$image}}" alt="Carousel Thumb" class="image-responsive">
                                            </li>  
                                        
                                    @endforeach

                           
                                @endif

                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">

                                @if(count($listing->images) > 0)
                                    @foreach($listing->images as $key => $image)
                                        <?php//echo $key . $image; ?>
                                        <!-- item -->
                                        <div class="item @php if($key == 0) echo 'active'; @endphp">
                                            <div class="carousel-image" style="max-height: 400px; min-height: 400px; ">
                                                <!-- image-wrapper -->
                                                <img src="{{'/storage/'.$image}}" alt="Featured Image" class="img-resp">
                                            </div>
                                        </div><!-- item -->
                                        
                                    @endforeach

                                    @else
                                    <div class="carousel-image">
                                    <img src="{{'/images/featured/2.jpg'}}" alt="Featured Image" class="img-resp">
                                    </div>
                                    
                                @endif

                            </div><!-- carousel-inner -->

                            <!-- Controls -->
                            <a class="left carousel-control" href="#product-carousel" role="button" data-slide="prev">
                                <i class="fa fa-chevron-left"></i>
                            </a>
                            <a class="right carousel-control" href="#product-carousel" role="button" data-slide="next">
                                <i class="fa fa-chevron-right"></i>
                            </a><!-- Controls -->
                        </div>
                    </div><!-- Controls --> 
      

                    <!-- slider-text -->
                    <aside class="col-lg-4" id="sidebar">
                        <div class="box_detail booking">
                            <div class="price">
                                <span>Contact {{ $listing->companyname }}</span>
                               
                            </div>

                         @guest
                         <a href="{{ url('/login') }}" class=" add_top_30 btn_1 full-width purchase">Message {{ $listing->companyname }}</a>
                         @else
                          <form action="{{ route('listings.contact.store', [$area, $listing]) }}" method="post">
                         <div class="form-group clearfix{{ $errors->has('message') ? ' has-error' : '' }}">
                            <textarea class="form-control add_bottom_15" name="message" id="message" placeholder="Message {{ $listing->companyname }} here!" style="height: 150px;"></textarea>

                             @if ($errors->has('message'))
                                    <span class="help-block">
                                        {{ $errors->first('message') }}
                                    </span>
                                @endif
                         </div>
                         <button type="submit" class="add_top_30 btn_1 full-width purchase">Send message</button>

                          {{ csrf_field() }}
                           </form>
                         @endguest  
                         <button id="show-address" class="btn_1 full-width outline wishlist"> <i class="fa fa-map-marker-alt" data-toggle="modal" 
                                    data-target="#mapModal" data-address="{{$listing->address}}"
                                    data-place-name="{{$listing->companyname}}"
                                    data-category="{{$listing->category->id}}"
                                    data-area="{{$area->slug}}"> View on Map</i> 
                         </button> 
                           
                            <div class="text-center"><small>We are using your registered email address to send</small></div>
                        </div>
                        <ul class="share-buttons">
                           <!-- Load Facebook SDK for JavaScript -->
   <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
                            <li><a class="fb-share" 
                                data-href="{{ route('listings.show', [$area, $listing]) }}"
                                data-layout="button_count">
                                <i class="fab fa-facebook fa-2x"></i>Share
                                </a>
                            </li>
                            <li><a class="twitter-share" href="#0"><i class="fab fa-twitter fa-2x"></i>Tweet</a></li>
                            <li><a class="gplus-share" href="#0"><i class="fas fa-envelope"></i> Email</a></li>
                        </ul>
                    </aside>             
                </div>              
            </div><!-- slider -->

           </section>  

                    <nav class="secondary_nav sticky_horizontal_2">
            <div class="container">
                <ul class="clearfix">
                    <li><a href="#description" class="active">Description</a></li>
                   
                </ul>
            </div>
        </nav>

        <div class="container margin_60_35">
                <div class="row">
                    <div class="col-lg-8">
                        <section id="description">
                            <div class="detail_title_1">
                                
                                <h1>{{ $listing->companyname }}</h1>
                                
                            </div>
                            <p>{{ $listing->body }}</p>
                          
                        </section>
                        <!-- /section -->
                    
                        
           
                </div>
                <!-- /row -->
        </div>
        <!-- /container -->
        
    </main>
    <!--/main-->

    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
})
    </script>
@endsection

@section('more-scripts')
    @php
        env('GOOGLE_MAPS_API_KEY') ? $mapsApiKey = '?key='.env('GOOGLE_MAPS_API_KEY') : $mapsApiKey = '';
    @endphp
    {{-- Load the maps js to show place on map--}}
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js{{$mapsApiKey}}"></script>
    <script src="/show-on-map.js"></script>
@endsection
