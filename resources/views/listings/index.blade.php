@extends('layouts.app')

@section('content')

@include('includes.banner')



 <div class="jobs-listing section-padding">
        <div class="container">
            <div class="job-topbar">
                <span class="pull-left">
                 
                
                 </span>
                
            </div>
           

            <div class="tab-content tr-job-posted">
                <div role="tabpanel" class="tab-pane fade in active two-column" id="two-column pull-right">
                    



    @if ($listings->count())
        @foreach ($listings as $listing)
            @include ('listings.partials._listing', compact('listing'))
        @endforeach


{{ $listings->links() }}
    @else
        <p>No listings found.</p>
    @endif
                     
                </div><!-- /.tab-pane -->


            </div><!-- /.tab-content -->        
        </div><!-- /.container -->      
    </div><!-- /.jobs-listing -->
@endsection
