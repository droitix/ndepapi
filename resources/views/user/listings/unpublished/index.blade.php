@extends('layouts.app')


@section('content')

  <div class="tr-breadcrumb bg-image section-before">
        <div class="container">
            <div class="breadcrumb-info text-center">
                <div class="user-image pull-left" style="margin-top:50px;" >
                    <img src="/uploads/avatars/{{ $user->avatar }}" alt="Image" class="img-responsive">
                </div>
                <div class="user-title">
                    <h1>{{ Auth::user()->name }}</h1>
                </div>      
                <ul class="job-meta tr-list list-inline">
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i>{{ $user->location }}</li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i>+263{{ $user->phone }}</li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#">{{ $user->email }}</a></li>
                    <li><i class="fa fa-briefcase" aria-hidden="true"></i>{{ $user->occupation }}</li>
                </ul>
                <ul class="breadcrumb-social social-icon-bg  tr-list">
                    <li><a href="https://facebook.com/{{ $user->facebook }}"><i class="fab fa-facebook"></i><span>facebook</span></a></li>
                    <li><a href="https://twitter.com/{{ $user->twitter }}"><i class="fab fa-twitter"></i> <span>twitter</span> </a></li>
                    <li><a href="https://instagram.com/{{ $user->instagram }}"><i class="fab fa-instagram"></i> <span>instagram</span> </a>
                    <li><a href="{{ $user->website }}"><i class="fas fa-globe"></i> <span>my website</span> </a></li>
                </ul>           
            </div>
        </div><!-- /.container -->
    </div><!-- /.tr-breadcrumb -->  

    <div class="tr-profile section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div class="tr-sidebar">
                       <ul class="nav nav-tabs profile-tabs section" role="tablist">
                            <li role="presentation" ><a href="#account-info" aria-controls="account-info" role="tab" data-toggle="tab"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Account Info</a></li>
                            
                            <li role="presentation"><a href="{{ route('listings.favourites.index', [$area]) }}" ><span><i class="fas fa-heart" aria-hidden="true"></i></span>Favourite listings</a></li>
                            <li role="presentation"><a href="{{ route('listings.published.index', [$area]) }}" ><span><i class="fa fa-clone" aria-hidden="true"></i></span>My listings</a></li>
                            
                           <li role="presentation" class="active"><a href="{{ route('listings.unpublished.index', [$area]) }}" ><span><i class="fas fa-drafting-compass"></i></span>Drafts</a></li>

                           <li role="presentation"><a href="{{ route('listings.viewed.index', [$area]) }}" ><span><i class="far fa-eye"></i></span>Viewed Listings</a></li>
                        </ul>   
                        <a href="#" class="btn btn-primary"><i class="far fa-file-pdf"></i><span>Download Registration Form</span></a>                         
                    </div><!-- /.tr-sidebar -->             
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane  account-info" id="account-info">    
                            <div class="tr-fun-fact">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="fun-fact">
                                            <div class="fun-fact-icon">
                                               <i class="fas fa-file-invoice-dollar fa-3x" ></i>
                                            </div>
                                            <div class="media-body">
                                                <h1 class="counter">$32.00</h1>
                                                <span>Revenue</span>
                                            </div>
                                        </div><!-- /.fun-fact -->
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="fun-fact">
                                            <div class="fun-fact-icon">
                                              <i class="fas fa-clipboard-list fa-3x"></i>
                                            </div>
                                            <div class="media-body">
                                                <h1 class="counter">12</h1>
                                                <span>My listings</span>
                                            </div>
                                        </div><!-- /.fun-fact -->                                   
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="fun-fact">
                                            <div class="fun-fact-icon">
                                                <i class="fas fa-spinner fa-3x"></i>
                                            </div>
                                            <div class="media-body">
                                                <h1 class="counter">5</h1>
                                                <span>Pending approval</span>
                                            </div>
                                        </div><!-- /.fun-fact -->
                                    </div>
                                </div><!-- ./row -->                            
                            </div><!-- /.tr-fun-fact -->

                           

                            <div class="section display-information">
                                <div class="title title-after">
                                    <div class="icon"><i class="fas fa-barcode fa-3x"></i></div> 
                                    <span>Your display Information</span>
                                </div>

                                <div class="change-photo">
                                    <div class="user-image">
                                        <img src="/uploads/avatars/{{ $user->avatar }}" alt="Image" class="img-responsive">
                                    </div>

                       <form enctype="multipart/form-data" style="margin-bottom:150px;" action="{{route('profile.update.avatar')}}" method="POST" id="avatarForm">                            
                            <input type="file" name="avatar">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="pull-right bt btn-sm btn-primary">
                        </form>

                     
                
                               <!-- form -->
                                   <form action="{{route('profile.update')}}" method="post">     {{ csrf_field() }}                    
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" name="name" class="form-control" value="{{ $user->name }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Full name</label>
                                        <input type="text" name="fullname" class="form-control" value="{{ $user->fullname }}" placeholder="Ex Paul Kumbweya">
                                    </div>
                                    <div class="form-group">
                                        <label>Occupation</label>
                                        <input type="text" name="occupation" class="form-control" value="{{ $user->occupation }}" placeholder="Ex Engineer">
                                    </div>


                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" value="{{ $user->email }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="name-three">Phone</label>
                                        <input type="text" name="phone" class="form-control" value="{{ $user->phone }}"  placeholder="Ex 772543219">
                                    </div>

                                    
                                    <div class="form-group">
                                        <label>Location</label>
                                        <input type="text"  name="location" class="form-control" value="{{ $user->location }}" placeholder="Ex Kuwadzana,Harare,Zimbabwe">
                                    </div> 
                                        <div class="form-group">
                                        <label>Facebook</label>
                                        <input type="text" name="facebook" class="form-control" value="{{ $user->facebook }}" placeholder="Ex drizzy101">
                                    </div>
                                    <div class="form-group">
                                        <label>Twitter</label>
                                        <input type="text" name="twitter" class="form-control" value="{{ $user->twitter }}" placeholder="Ex drizzy2">
                                    </div>
                                    <div class="form-group">
                                        <label>Instagram</label>
                                        <input type="text" name="instagram" class="form-control" value="{{ $user->instagram }}" placeholder="Ex paula2013">
                                    </div>


                                    <div class="form-group">
                                        <label>Website</label>
                                        <input type="text" name="website" class="form-control" value="{{ $user->website }}" placeholder="Ex https://ndepapi.com">
                                    </div> 
                                 <div class="form-group" style="text-align:center;">
                                    <button type="submit" class="btn btn-sm btn-primary" style="width:50%;">Update Details</button>
                                </div>
                                         
                                </div><!-- profile-details -->
                                
                                
                            </form>                            
                            </div><!-- /.display-information -->
                           
                        </div><!-- /.tab-pane -->

                        <div role="tabpanel" class="tab-pane  bookmark" id="bookmark">
                            <div class="row">
                                <div class="col-md-4 remove-item">
                                    <div class="job-item">
                                        <span class="remove-icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <div class="item-overlay">
                                            <div class="job-info">
                                                <a href="#" class="btn btn-primary">Full Time</a>
                                                <span class="tr-title">
                                                    <a href="job-details.html">Project Manager</a>
                                                    <span><a href="#">Dig File</a></span>
                                                </span>
                                                <ul class="tr-list job-meta">
                                                    <li><i class="fa fa-map-signs" aria-hidden="true"></i>San Francisco, CA, US</li>
                                                    <li><i class="fa fa-briefcase" aria-hidden="true"></i>Mid Level</li>
                                                    <li><i class="fa fa-money" aria-hidden="true"></i>$5,000 - $6,000</li>
                                                </ul>
                                                <ul class="job-social tr-list">
                                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-expand" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="job-details.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>                                      
                                        </div>
                                        <div class="job-info">
                                            <div class="company-logo">
                                                <img src="images/job/1.png" alt="images" class="img-responsive">
                                            </div>
                                            <span class="tr-title">
                                                <a href="#">Project Manager</a>
                                                <span><a href="#">Dig File</a></span>
                                            </span>
                                            <ul class="tr-list job-meta">
                                                <li><span><i class="fa fa-map-signs" aria-hidden="true"></i></span>San Francisco, CA, US</li>
                                                <li><span><i class="fa fa-briefcase" aria-hidden="true"></i></span>Mid Level</li>
                                                <li><span><i class="fa fa-money" aria-hidden="true"></i></span>$5,000 - $6,000</li>
                                            </ul>
                                            <div class="time">
                                                <a href="#"><span>Full Time</span></a>
                                                <span class="pull-right">Posted 23 hours ago</span>
                                            </div>                          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 remove-item">
                                    <div class="job-item">
                                        <span class="remove-icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <div class="item-overlay">
                                            <div class="job-info">
                                                <a href="job-details.html" class="btn btn-primary">Part Time</a>
                                                <span class="tr-title">
                                                    <a href="job-details.html">Design Associate</a>
                                                    <span><a href="#">Loop</a></span>
                                                </span>
                                                <ul class="tr-list job-meta">
                                                    <li><i class="fa fa-map-signs" aria-hidden="true"></i>San Francisco, CA, US</li>
                                                    <li><i class="fa fa-briefcase" aria-hidden="true"></i>Mid Level</li>
                                                    <li><i class="fa fa-money" aria-hidden="true"></i>$5,000 - $6,000</li>
                                                </ul>
                                                <ul class="job-social tr-list">
                                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-expand" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="job-details.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>                                      
                                        </div>                              
                                        <div class="job-info">
                                            <div class="company-logo">
                                                <img src="images/job/2.png" alt="images" class="img-responsive">
                                            </div>
                                            <span class="tr-title">
                                                <a href="#">Design Associate</a>
                                                <span><a href="#">Loop</a></span>
                                            </span>
                                            <ul class="tr-list job-meta">
                                                <li><span><i class="fa fa-map-signs" aria-hidden="true"></i></span>San Francisco, CA, US</li>
                                                <li><span><i class="fa fa-briefcase" aria-hidden="true"></i></span>Mid Level</li>
                                                <li><span><i class="fa fa-money" aria-hidden="true"></i></span>$5,000 - $6,000</li>
                                            </ul>
                                            <div class="time">
                                                <a href="#"><span class="part-time">Part Time</span></a>
                                                <span class="pull-right">Posted 1 day ago</span>
                                            </div>          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 remove-item">
                                    <div class="job-item">
                                        <span class="remove-icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <div class="item-overlay">
                                            <div class="job-info">
                                                <a href="#" class="btn btn-primary">Freelance</a>
                                                <span class="tr-title">
                                                    <a href="job-details.html">Graphic Designer</a>
                                                    <span><a href="#">Humanity Creative</a></span>
                                                </span>
                                                <ul class="tr-list job-meta">
                                                    <li><i class="fa fa-map-signs" aria-hidden="true"></i>San Francisco, CA, US</li>
                                                    <li><i class="fa fa-briefcase" aria-hidden="true"></i>Mid Level</li>
                                                    <li><i class="fa fa-money" aria-hidden="true"></i>$5,000 - $6,000</li>
                                                </ul>
                                                <ul class="job-social tr-list">
                                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-expand" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="job-details.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>                                      
                                        </div>                              
                                        <div class="job-info">
                                            <div class="company-logo">
                                                <img src="images/job/3.png" alt="images" class="img-responsive">
                                            </div>
                                            <span class="tr-title">
                                                <a href="#">Graphic Designer</a>
                                                <span><a href="#">Humanity Creative</a></span>
                                            </span>
                                            <ul class="tr-list job-meta">
                                                <li><span><i class="fa fa-map-signs" aria-hidden="true"></i></span>San Francisco, CA, US</li>
                                                <li><span><i class="fa fa-briefcase" aria-hidden="true"></i></span>Mid Level</li>
                                                <li><span><i class="fa fa-money" aria-hidden="true"></i></span>$5,000 - $6,000</li>
                                            </ul>
                                            <div class="time">
                                                <a href="#"><span class="freelance">Freelance</span></a>
                                                <span class="pull-right">Posted 10 day ago</span>
                                            </div>          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 remove-item">
                                    <div class="job-item">
                                        <span class="remove-icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <div class="item-overlay">
                                            <div class="job-info">
                                                <a href="#" class="btn btn-primary">Full Time</a>
                                                <span class="tr-title">
                                                    <a href="job-details.html">Design Consultant</a>
                                                    <span><a href="#">Families</a></span>
                                                </span>
                                                <ul class="tr-list job-meta">
                                                    <li><i class="fa fa-map-signs" aria-hidden="true"></i>San Francisco, CA, US</li>
                                                    <li><i class="fa fa-briefcase" aria-hidden="true"></i>Mid Level</li>
                                                    <li><i class="fa fa-money" aria-hidden="true"></i>$5,000 - $6,000</li>
                                                </ul>
                                                <ul class="job-social tr-list">
                                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-expand" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="job-details.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>                                      
                                        </div>                              
                                        <div class="job-info">
                                            <div class="company-logo">
                                                <img src="images/job/4.png" alt="images" class="img-responsive">
                                            </div>
                                            <span class="tr-title">
                                                <a href="#">Design Consultant</a>
                                                <span><a href="#">Families</a></span>
                                            </span>
                                            <ul class="tr-list job-meta">
                                                <li><span><i class="fa fa-map-signs" aria-hidden="true"></i></span>San Francisco, CA, US</li>
                                                <li><span><i class="fa fa-briefcase" aria-hidden="true"></i></span>Mid Level</li>
                                                <li><span><i class="fa fa-money" aria-hidden="true"></i></span>$5,000 - $6,000</li>
                                            </ul>
                                            <div class="time">
                                                <a href="#"><span>Full Time</span></a>
                                                <span class="pull-right">Posted Oct 09, 2017</span>
                                            </div>              
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 remove-item">
                                    <div class="job-item">
                                        <span class="remove-icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <div class="item-overlay">
                                            <div class="job-info">
                                                <a href="#" class="btn btn-primary">Part Time</a>
                                                <span class="tr-title">
                                                    <a href="job-details.html">Project Manager</a>
                                                    <span><a href="#">Sky Creative</a></span>
                                                </span>
                                                <ul class="tr-list job-meta">
                                                    <li><i class="fa fa-map-signs" aria-hidden="true"></i>San Francisco, CA, US</li>
                                                    <li><i class="fa fa-briefcase" aria-hidden="true"></i>Mid Level</li>
                                                    <li><i class="fa fa-money" aria-hidden="true"></i>$5,000 - $6,000</li>
                                                </ul>
                                                <ul class="job-social tr-list">
                                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-expand" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="job-details.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>                                      
                                        </div>                              
                                        <div class="job-info">
                                            <div class="company-logo">
                                                <img src="images/job/5.png" alt="images" class="img-responsive">
                                            </div>
                                            <span class="tr-title">
                                                <a href="#">Project Manager</a>
                                                <span><a href="#">Sky Creative</a></span>
                                            </span>
                                            <ul class="tr-list job-meta">
                                                <li><span><i class="fa fa-map-signs" aria-hidden="true"></i></span>San Francisco, CA, US</li>
                                                <li><span><i class="fa fa-briefcase" aria-hidden="true"></i></span>Mid Level</li>
                                                <li><span><i class="fa fa-money" aria-hidden="true"></i></span>$5,000 - $6,000</li>
                                            </ul>   
                                            <div class="time">
                                                <a href="#"><span class="part-time">Part Time</span></a>
                                                <span class="pull-right">Posted 1 day ago</span>
                                            </div>          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 remove-item">
                                    <div class="job-item">
                                        <span class="remove-icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <div class="item-overlay">
                                            <div class="job-info">
                                                <a href="#" class="btn btn-primary">Freelance</a>
                                                <span class="tr-title">
                                                    <a href="job-details.html">Design Associate</a>
                                                    <span><a href="#">Pencil</a></span>
                                                </span>
                                                <ul class="tr-list job-meta">
                                                    <li><i class="fa fa-map-signs" aria-hidden="true"></i>San Francisco, CA, US</li>
                                                    <li><i class="fa fa-briefcase" aria-hidden="true"></i>Mid Level</li>
                                                    <li><i class="fa fa-money" aria-hidden="true"></i>$5,000 - $6,000</li>
                                                </ul>
                                                <ul class="job-social tr-list">
                                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-expand" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="job-details.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>                                      
                                        </div>                              
                                        <div class="job-info">
                                            <div class="company-logo">
                                                <img src="images/job/6.png" alt="images" class="img-responsive">
                                            </div>
                                            <span class="tr-title">
                                                <a href="#">Design Associate</a>
                                                <span><a href="#">Pencil</a></span>
                                            </span>
                                            <ul class="tr-list job-meta">
                                                <li><span><i class="fa fa-map-signs" aria-hidden="true"></i></span>San Francisco, CA, US</li>
                                                <li><span><i class="fa fa-briefcase" aria-hidden="true"></i></span>Mid Level</li>
                                                <li><span><i class="fa fa-money" aria-hidden="true"></i></span>$5,000 - $6,000</li>
                                            </ul>
                                            <div class="time">
                                                <a href="#"><span class="freelance">Freelance</span></a>
                                                <span class="pull-right">Posted 23 hours ago</span>
                                            </div>              
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 remove-item">
                                    <div class="job-item">
                                        <span class="remove-icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <div class="item-overlay">
                                            <div class="job-info">
                                                <a href="#" class="btn btn-primary">Full Time</a>
                                                <span class="tr-title">
                                                    <a href="job-details.html">Graphic Designer</a>
                                                    <span><a href="#">Fox</a></span>
                                                </span>
                                                <ul class="tr-list job-meta">
                                                    <li><i class="fa fa-map-signs" aria-hidden="true"></i>San Francisco, CA, US</li>
                                                    <li><i class="fa fa-briefcase" aria-hidden="true"></i>Mid Level</li>
                                                    <li><i class="fa fa-money" aria-hidden="true"></i>$5,000 - $6,000</li>
                                                </ul>
                                                <ul class="job-social tr-list">
                                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-expand" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="job-details.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>                                      
                                        </div>                              
                                        <div class="job-info">
                                            <div class="company-logo">
                                                <img src="images/job/7.png" alt="images" class="img-responsive">
                                            </div>
                                            <span class="tr-title">
                                                <a href="#">Graphic Designer</a>
                                                <span><a href="#">Fox</a></span>
                                            </span>
                                            <ul class="tr-list job-meta">
                                                <li><span><i class="fa fa-map-signs" aria-hidden="true"></i></span>San Francisco, CA, US</li>
                                                <li><span><i class="fa fa-briefcase" aria-hidden="true"></i></span>Mid Level</li>
                                                <li><span><i class="fa fa-money" aria-hidden="true"></i></span>$5,000 - $6,000</li>
                                            </ul>
                                            <div class="time">
                                                <a href="#"><span>Full Time</span></a>
                                                <span class="pull-right">Posted Oct 09, 2017</span>
                                            </div>              
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 remove-item">
                                    <div class="job-item">
                                        <span class="remove-icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <div class="item-overlay">
                                            <div class="job-info">
                                                <a href="#"><span class="btn btn-primary">Part Time</span></a>
                                                <span class="tr-title">
                                                    <a href="job-details.html">Design Consultant</a>
                                                    <span><a href="#">Owl</a></span>
                                                </span>
                                                <ul class="tr-list job-meta">
                                                    <li><i class="fa fa-map-signs" aria-hidden="true"></i>San Francisco, CA, US</li>
                                                    <li><i class="fa fa-briefcase" aria-hidden="true"></i>Mid Level</li>
                                                    <li><i class="fa fa-money" aria-hidden="true"></i>$5,000 - $6,000</li>
                                                </ul>
                                                <ul class="job-social tr-list">
                                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-expand" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a></li>
                                                    <li><a href="job-details.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>                                      
                                        </div>                              
                                        <div class="job-info">
                                            <div class="company-logo">
                                                <img src="images/job/8.png" alt="images" class="img-responsive">
                                            </div>
                                            <span class="tr-title">
                                                <a href="#">Design Consultant</a>
                                                <span><a href="#">Owl</a></span>
                                            </span>
                                            <ul class="tr-list job-meta">
                                                <li><span><i class="fa fa-map-signs" aria-hidden="true"></i></span>San Francisco, CA, US</li>
                                                <li><span><i class="fa fa-briefcase" aria-hidden="true"></i></span>Mid Level</li>
                                                <li><span><i class="fa fa-money" aria-hidden="true"></i></span>$5,000 - $6,000</li>
                                            </ul>
                                            <div class="time">
                                                <a href="#"><span class="part-time">Part Time</span></a>
                                                <span class="pull-right">Posted 10 day ago</span>
                                            </div>          
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.row -->                            
                        </div><!-- /.tab-pane -->

                        

                        <div role="tabpanel" class="tab-pane fade in active apply-job" id="archived">
                             @if ($listings->count())
        @each ('listings.partials._unpublished', $listings, 'listing')
        {{ $listings->links() }}
    @else
        <p>No unpublished listings.</p>
    @endif
                        </div><!-- /.tab-pane -->
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.tr-profile --> 



@endsection
