<?php

use Illuminate\Database\Seeder;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [
            [
            'name' => 'ZIM',
                'children' => [
                    [
                        'name' => 'Beitbridge',
                        'icon' => 'fa-torii-gate',
                        'color' => 'FF6F59',
                        'children' => [
                            ['name' => 'CBD'],
                            ['name' => 'Dulibadzimu'],
                        ],
                    ],
                    [
                        'name' => 'Bindura',
                        'icon' => 'fa-road',
                        'color' => '020202',
                        'children' => [
                            ['name' => 'Aerodrome'],
                            ['name' => 'Bindura suburb'],
                            ['name' => 'CBD'],
                            ['name' => 'Chipadze'],
                            ['name' => 'Chiwaridzo'],
                            ['name' => 'Garikai'],
                        ],
                    ],
                    [
                        'name' => 'Bulawayo',
                        'icon' => 'fa-industry',
                        'color' => 'D5B942',
                        'children' => [
                            ['name' => 'Ascot'],
                            ['name' => 'Barbour Fields'],
                            ['name' => 'Barham Green'],
                            ['name' => 'Beacon Hill'],
                            ['name' => 'Bellevue'],
                            ['name' => 'Belmont'],
                            ['name' => 'Bradfield'],
                            ['name' => 'CBD'],
                            ['name' => 'Cowdray park'],
                            ['name' => 'Donnington'],
                            ['name' => 'Douglasdale'],
                            ['name' => 'Eloana'],
                            ['name' => 'Emakhandeni'],
                            ['name' => 'Famona'],
                            ['name' => 'Glencoe'],
                            ['name' => 'Glengary'],
                            ['name' => 'Greenhill'],
                            ['name' => 'Gwabalanda'],
                            ['name' => 'Helensvale'],
                            ['name' => 'Hillcrest'],
                            ['name' => 'Hume Park'],
                            ['name' => 'Hyde Park'],
                            ['name' => 'Ilanda'],
                            ['name' => 'Iminyela'],
                            ['name' => 'Intini'],
                            ['name' => 'Jacaranda'],
                            ['name' => 'Kelvin'],
                            ['name' => 'Kenilworth'],
                            ['name' => 'Khumalo'],
                            ['name' => 'Killarney'],
                            ['name' => 'Kingsdale'],
                            ['name' => 'Lakeside'],
                            ['name' => 'Lobengula'],
                            ['name' => 'Lochview'],
                            ['name' => 'Luveve'],
                            ['name' => 'Mabutweni'],
                            ['name' => 'Magwegwe'],
                            ['name' => 'Mahatshula'],
                            ['name' => 'Makokoba'],
                            ['name' => 'Malindela'],
                            ['name' => 'Manningdale'],
                            ['name' => 'Marlands'],
                            ['name' => 'Matsheumlope'],
                            ['name' => 'Montgomery'],
                            ['name' => 'Montrose'],
                            ['name' => 'Morningside'],
                            ['name' => 'Mpompoma'],
                            ['name' => 'Munda'],
                            ['name' => 'Mzilikazi'],
                            ['name' => 'Northvale'],
                            ['name' => 'Paddonhurst'],
                            ['name' => 'Parklands'],
                            ['name' => 'Parkview'],
                            ['name' => 'Pelandaba'],
                            ['name' => 'Pumula'],
                            ['name' => 'Queens Park'],
                            ['name' => 'Quensdale'],
                            ['name' => 'Rangemore'],
                            ['name' => 'Raylton'],
                            ['name' => 'Richmond'],
                            ['name' => 'Riverside'],
                            ['name' => 'Romney Park'],
                            ['name' => 'Selbourne Park'],
                            ['name' => 'Sizinda'],
                            ['name' => 'Southdale'],
                            ['name' => 'Southwold'],
                            ['name' => 'Steeldale'],
                            ['name' => 'Suburbs'],
                            ['name' => 'Sunninghill'],
                            ['name' => 'Sunnyside'],
                            ['name' => 'Thorngrove'],
                            ['name' => 'Trenace'],
                            ['name' => 'Tshabalala'],
                            ['name' => 'Umguza'],
                            ['name' => 'Waterford'],
                            ['name' => 'Waterlea'],
                            ['name' => 'Westgate'],
                            ['name' => 'Westondale'],
                            ['name' => 'Willsgrove'],
                            ['name' => 'Windsor Park'],
                            ['name' => 'Woodlands'],
                            ['name' => 'Woodville'],
                            ['name' => 'Woodville Park'],
                        ],
                    ],
                    [
                        'name' => 'Chegutu',
                        'icon' => 'fa-bus-alt',
                        'color' => '482728',
                        'children' => [
                            ['name' => 'CBD'],
                            ['name' => 'Chegutu Location'],
                            ['name' => 'Chestgate'],
                            ['name' => 'HintonVille'],
                            ['name' => 'Kaguvi'],
                            ['name' => 'Mvovo'],
                            ['name' => 'Pfupajena'],
                            ['name' => 'Riffle Range'],
                            ['name' => 'ZMDC'],
                            
                        ],
                    ],
                    [
                        'name' => 'Chinhoyi',
                        'icon' => 'fa-map-signs',
                        'color' => '494368',
                        'children' => [
                            ['name' => 'Brundish'],
                            ['name' => 'CBD'],
                            ['name' => 'Chikonohono'],
                            ['name' => 'Cold stream'],
                            ['name' => 'Gazema'],
                            ['name' => 'Gunhill'],
                            ['name' => 'Hunyani'],
                            ['name' => 'Mupata Section'],
                            ['name' => 'Muzare'],
                            ['name' => 'Orange Groove'],
                            ['name' => 'Ruvimbo'],

                           ], 
                        ],
                        [
                        'name' => 'Chipinge',
                        'icon' => 'fa-bolt',
                        'color' => 'BC2C1A',
                        'children' => [
                            ['name' => 'CBD'],
                            ['name' => 'Gaza Township'],
                            ['name' => 'Low Density'],
                            ['name' => 'Medium Density'],
                            ['name' => 'Usanga'],

                           ], 
                        ],
                        [
                        'name' => 'Chiredzi',
                        'icon' => 'fa-coffee',
                        'color' => '554640',
                        'children' => [
                            ['name' => 'Buffalo Range'],
                            ['name' => 'CBD'],
                            ['name' => 'Hippo Valley'],
                            ['name' => 'Malilangwe'],
                            ['name' => 'Mukwasini'],
                            ['name' => 'Nandi'],
                            ['name' => 'Town'],
                            ['name' => 'Tshovane'],
                            ['name' => 'ZSA'],
                           
                            ],
                        ],
                        [
                        'name' => 'Chitungwiza',
                        'icon' => 'fa-fighter-jet',
                        'color' => '0075A2',
                        'children' => [
                            ['name' => 'Makoni'],
                            ['name' => 'Manyame Park'],
                            ['name' => 'Mayambara'],
                            ['name' => 'Nyatsime'],
                            ['name' => 'Rockview'],
                            ['name' => 'Seke'],
                            ['name' => 'St Marys'],
                            ['name' => 'Zengeza'],
                            ['name' => 'Ziko'],
                           
                            ],
                        ],
                        [
                        'name' => 'Gwanda',
                        'icon' => 'fa-traffic-light',
                        'color' => 'E01A4F',
                        'children' => [
                            ['name' => 'CBD'],
                            ['name' => 'Geneva'],
                            ['name' => 'Jacaranda'],
                            ['name' => 'Jahunda'],
                            ['name' => 'Marriage'],
                            ['name' => 'Phakama'],
                            ['name' => 'Spitzkop'],
                            ['name' => 'Ultra High'],
                            
                            ],
                        ],
                        [
                        'name' => 'Gweru',
                        'icon' => 'fa-landmark',
                        'color' => 'FFA630',
                        'children' => [
                            ['name' => 'Ascot'],
                            ['name' => 'Athlone'],
                            ['name' => 'Brackenhurst'],
                            ['name' => 'CBD'],
                            ['name' => 'Christmas Gift'],
                            ['name' => 'Clifton Park'],
                            ['name' => 'Daylesford'],
                            ['name' => 'Enfield'],
                            ['name' => 'Gweru east'],
                            ['name' => 'Hertfordshire'],
                            ['name' => 'Ivene'],
                            ['name' => 'Kopje'],
                            ['name' => 'Lundi Park'],
                            ['name' => 'Mambo'],
                            ['name' => 'Mkoba'],
                            ['name' => 'Mtapa'],
                            ['name' => 'Mtausi Park'],
                            ['name' => 'Munhumutapa'],
                            ['name' => 'Nashville'],
                            ['name' => 'Nehosho'],
                            ['name' => 'Northgate'],
                            ['name' => 'Northlea'],
                            ['name' => 'Ridgemond'],
                            ['name' => 'Riverside'],
                            ['name' => 'Rundolf Park'],
                            ['name' => 'Senga'],
                            ['name' => 'Shamrock Park'],
                            ['name' => 'Sithabile'],
                            ['name' => 'Southdowns'],
                            ['name' => 'Southview'],
                            ['name' => 'St Annes Drive'],
                            ['name' => 'Windsor Park'],
                            ['name' => 'Woodlands'],

                            ],
                        ],
                       [
                        'name' => 'Harare',
                        'icon' => 'fa-city',
                        'color' => '0F1A20',
                        'children' => [
                            ['name' => 'Adylin'],
                            ['name' => 'Alexandra Park'],
                            ['name' => 'Amby'],
                            ['name' => 'Arcadia'],
                            ['name' => 'Ardbennie'],
                            ['name' => 'Arlingon'],
                            ['name' => 'Ashbrittle'],
                            ['name' => 'Ashdown Park'],
                            ['name' => 'Athlone'],
                            ['name' => 'Avenues'],
                            ['name' => 'Avondale'],
                            ['name' => 'Avondale West'],
                            ['name' => 'Avonlea'],
                            ['name' => 'Ballantyne Park'],
                            ['name' => 'Belgravia'],
                            ['name' => 'Belvedere'],
                            ['name' => 'Beverley'],
                            ['name' => 'Bloomingdale'],
                            ['name' => 'Bluffhill'],
                            ['name' => 'Borrowdale'],
                            ['name' => 'Borrowdale Brook'],
                            ['name' => 'Borrowdale West'],
                            ['name' => 'Braeside'],
                            ['name' => 'Riverside'],
                            ['name' => 'Brook Ridge'],
                            ['name' => 'Budiriro'],
                            ['name' => 'Carrick Creagh'],
                            ['name' => 'CBD'],
                            ['name' => 'Chadcombe'],
                            ['name' => 'Chikurubi'],
                            ['name' => 'Chipukutu'],
                            ['name' => 'Chiremba Park'],
                            ['name' => 'Chisipiti'],
                            ['name' => 'Chisipite'],
                            ['name' => 'Chizhanje'],
                            ['name' => 'City Centre'],
                            ['name' => 'Civic Centre'],
                            ['name' => 'Cold Comfort'],
                            ['name' => 'Colne Valley'],
                            ['name' => 'Colray'],
                            ['name' => 'Coronation Park'],
                            ['name' => 'Cotsworld Hills'],
                            ['name' => 'Cranbourne Park'],
                            ['name' => 'Crowborough'],
                            ['name' => 'Damofalls'],
                            ['name' => 'Dawn Hill'],
                            ['name' => 'Donnybrook'],
                            ['name' => 'Dzivarasekwa'],
                            ['name' => 'Estlea'],
                            ['name' => 'Eastview'],
                            ['name' => 'Emerald Hill'],
                            ['name' => 'Epworth'],
                            ['name' => 'Gevstein Park'],
                            ['name' => 'Glaudina'],
                            ['name' => 'Glen lorne'],
                            ['name' => 'Glen Norah'],
                            ['name' => 'Glen View'],
                            ['name' => 'Glenwood'],
                            ['name' => 'Grange'],
                            ['name' => 'Greystone Park'],
                            ['name' => 'Grobbie Park'],
                            ['name' => 'Groombridge'],
                            ['name' => 'Gun Hill'],
                            ['name' => 'Haig Park'],
                            ['name' => 'Hatcliffe'],
                            ['name' => 'Hatfield'],
                            ['name' => 'Helensvale'],
                            ['name' => 'Highfield'],
                            ['name' => 'Highlands'],
                            ['name' => 'Hillside'],
                            ['name' => 'Hogerty Hill'],
                            ['name' => 'Hopely'],
                            ['name' => 'Houghton Park'],
                            ['name' => 'Induna'],
                            ['name' => 'Kambanje'],
                            ['name' => 'Kambuzuma'],
                            ['name' => 'Kensington'],
                            ['name' => 'Kopje'],
                            ['name' => 'Kutsaga'],
                            ['name' => 'Kuwadzana'],
                            ['name' => 'Letombo Park'],
                            ['name' => 'Lewisam'],
                            ['name' => 'Lichendale'],
                            ['name' => 'Lincoln Green'],
                            ['name' => 'Little Norfolk'],
                            ['name' => 'Lochinvar'],
                            ['name' => 'Lorelei'],
                            ['name' => 'Luna'],
                            ['name' => 'Mabelreign'],
                            ['name' => 'Mabvuku'],
                            ['name' => 'Mainway Meadows'],
                            ['name' => 'Malvern'],
                            ['name' => 'Mandara'],
                            ['name' => 'Manidoda Park'],
                            ['name' => 'Manresa'],
                            ['name' => 'Marimba Park'],
                            ['name' => 'Malborough'],
                            ['name' => 'Mayfield Park'],
                            ['name' => 'Mbare'],
                            ['name' => 'Meyrick Park'],
                            ['name' => 'Midlands'],
                            ['name' => 'Milton Park'],
                            ['name' => 'Mondora'],
                            ['name' => 'Monovale'],
                            ['name' => 'Mount Hampden'],
                            ['name' => 'Mount Pleasant'],
                            ['name' => 'Msasa'],
                            ['name' => 'Msasa Park'],
                            ['name' => 'Mkoba'],
                            ['name' => 'Mufakose'],
                            ['name' => 'Mukuvisi'],
                            ['name' => 'Mukuvisi Park'],
                            ['name' => 'New Ridgeview'],
                            ['name' => 'Newlands'],
                            ['name' => 'Nwisi Park'],
                            ['name' => 'Northwood'],
                            ['name' => 'Old forest'],
                            ['name' => 'Park Meadowlands'],
                            ['name' => 'Parktown'],
                            ['name' => 'Philadelphia'],
                            ['name' => 'Pomona'],
                            ['name' => 'Prospect'],
                            ['name' => 'Queensdale'],
                            ['name' => 'Quinnington'],
                            ['name' => 'Rhodesville'],
                            ['name' => 'Ridgeview'],
                            ['name' => 'Rietfontein'],
                            ['name' => 'Rolf Valley'],
                            ['name' => 'Rugare'],
                            ['name' => 'Runniville'],
                            ['name' => 'Ryelands'],
                            ['name' => 'Sanganai Park'],
                            ['name' => 'Science Park'],
                            ['name' => 'Sentosa'],
                            ['name' => 'Shawasha Hills'],
                            ['name' => 'Sherwood Park'],
                            ['name' => 'Shortson'],
                            ['name' => 'Southerton'],
                            ['name' => 'St Andrews Park'],
                            ['name' => 'St Martins'],
                            ['name' => 'Strathaven'],
                            ['name' => 'Sunningdale'],
                            ['name' => 'Sunridge'],
                            ['name' => 'Sunrise'],
                            ['name' => 'Sunway City'],
                            ['name' => 'Tafara'],
                            ['name' => 'The Gradge'],
                            ['name' => 'Umwingsdale'],
                            ['name' => 'Uplands'],
                            ['name' => 'Vainona'],
                            ['name' => 'Valencedene'],
                            ['name' => 'Ventersburg'],
                            ['name' => 'Warren Park'],
                            ['name' => 'Waterfalls'],
                            ['name' => 'Westgate'],
                            ['name' => 'Westwood'],
                            ['name' => 'Willowvale'],
                            ['name' => 'Willmington Park'],
                            ['name' => 'Workington'],
                            ['name' => 'Zimre Park'],
                            
                            ],
                        ],
                        [
                        'name' => 'Kadoma',
                        'icon' => 'fa-tshirt',
                        'color' => '9CC4B2',
                        'children' => [
                            ['name' => 'CBD'],
                            ['name' => 'Chakari'],
                            ['name' => 'Cotton Research'],
                            ['name' => 'Eastview'],
                            ['name' => 'Eiffel Flats'],
                            ['name' => 'Ingezi'],
                            ['name' => 'Mornington'],
                            ['name' => 'Patchway'],
                            ['name' => 'Rimuka'],
                            ['name' => 'Rio Tinto'],
                            ['name' => 'Waverly'],
                            ['name' => 'Westview'],
                           
                            ],
                        ],
                        [
                        'name' => 'Kariba',
                        'icon' => 'fa-fish',
                        'color' => 'DFDFC1',
                        'children' => [
                            ['name' => 'Aerial Hill'],
                            ['name' => 'Baobab Ridge'],
                            ['name' => 'Batonga'],
                            ['name' => 'Bouldar Ridge'],
                            ['name' => 'CBD'],
                            ['name' => 'Heights'],
                            ['name' => 'Hospital Hill'],
                            ['name' => 'Mica Point'],
                            ['name' => 'Nyamhunga'],

                           ],   
                        ],
                        [
                        'name' => 'Karoi',
                        'icon' => 'fa-tractor',
                        'color' => '6DA34D',
                        'children' => [
                            ['name' => 'CBD'],
                            ['name' => 'Chiedza'],
                            ['name' => 'Chikangwe'],
                            ['name' => 'Flamboyant'],

                           ],
                        ],
                        [
                        'name' => 'Kwekwe',
                        'icon' => 'fa-compress',
                        'color' => 'B4436C',
                        'children' => [
                            ['name' => 'Amaveni'],
                            ['name' => 'CBD'],
                            ['name' => 'Fitchley'],
                            ['name' => 'Gaika'],
                            ['name' => 'Glenwood'],
                            ['name' => 'Golden Acres'],
                            ['name' => 'Masasa'],
                            ['name' => 'Mbizo'],
                            ['name' => 'New Town'],
                            ['name' => 'Redcliff'],
                            ['name' => 'Rutendo'],
                            ['name' => 'Towhood'],
                            ['name' => 'Westend'],
                            
                            ],  
                        ],
                        [
                        'name' => 'Marondera',
                        'icon' => 'fa-tree',
                        'color' => '0B7A75',
                        'children' => [
                            ['name' => '1st Street'],
                            ['name' => '2nd Street'],
                            ['name' => '4th Street'],
                            ['name' => 'CBD'],
                            ['name' => 'Cherima/Rujeko'],
                            ['name' => 'Cherutombo'],
                            ['name' => 'Dombotombo'],
                            ['name' => 'Garikai/Elveshood'],
                            ['name' => 'Nyameni'],
                            ['name' => 'Paradise'],
                            ['name' => 'Ruzawi Park'],
                            ['name' => 'Winston Park'],
                            ['name' => 'Yellow City'],
                            
                            ],  
                        ],
                        [
                        'name' => 'Masvingo',
                        'icon' => 'fa-chess-rook',
                        'color' => '22222F',
                        'children' => [
                            ['name' => 'Buffalo Range'],
                            ['name' => 'CBD'],
                            ['name' => 'Clipsharm Park'],
                            ['name' => 'Eastville'],
                            ['name' => 'Infantry Battalion'],
                            ['name' => 'Morningside'],
                            ['name' => 'Mucheke'],
                            ['name' => 'Rhodene'],
                            ['name' => 'Rujene'],
                            ['name' => 'Runyararo'],
                           
                           ],  
                        ],
                        [
                        'name' => 'Mutare',
                        'icon' => 'fa-traffic-light',
                        'color' => 'E01A4F',
                        'children' => [
                            ['name' => 'Avenues'],
                            ['name' => 'Bordervale'],
                            ['name' => 'CBD'],
                            ['name' => 'Chikanga'],
                            ['name' => 'Chikanga Extenstion'],
                            ['name' => 'Dangamvura'],
                            ['name' => 'Darlington'],
                            ['name' => 'Fern Valley'],
                            ['name' => 'Garikai'],
                            ['name' => 'Greenside Extension'],
                            ['name' => 'Hobhouse'],
                            ['name' => 'Mai Maria'],
                            ['name' => 'Morningside'],
                            ['name' => 'Murambi'],
                            ['name' => 'Natview Park'],
                            ['name' => 'Palmerston'],
                            ['name' => 'Sakubva'],
                            ['name' => 'St Josephs'],
                            ['name' => 'Tigers Kloof'],
                            ['name' => 'Toronto'],
                            ['name' => 'Utopia'],
                            ['name' => 'Weirmouth'],
                            ['name' => 'Wetslea'],
                            ['name' => 'Yeovil'],
                            ['name' => 'Zimta'],
                            ['name' => 'Zimunya'],

                           ],  
                        ],
                        [
                        'name' => 'Nyanga',
                        'icon' => 'fa-cloud-rain',
                        'color' => '009FB7',
                        'children' => [
                            ['name' => 'CBD'],
                            ['name' => 'Depe park'],
                            ['name' => 'Mangondoza'],
                            ['name' => 'Nyamuka'],
                            ['name' => 'Nyangani'],
                            ['name' => 'Rochdale'],
                            
                           ],  
                        ],
                        [
                        'name' => 'Plumtree',
                        'icon' => 'fa-subway',
                        'color' => 'BA9790',
                        'children' => [
                            ['name' => 'CBD'],
                            ['name' => 'Dingumuzi'],
                            ['name' => 'Hebron'],
                            ['name' => 'Kariba'],
                            ['name' => 'Lakeview'],
                            ['name' => 'Madubes'],
                            ['name' => 'Mathendele'],
                            ['name' => 'Matiwana'],
                            ['name' => 'Medium Density'],
                            ['name' => 'Rangiore'],
                            ['name' => 'ZBS'],
                            
                            ], 
                        ],
                        [
                        'name' => 'Rusape',
                        'icon' => 'fa-leaf',
                        'color' => 'CEF7A0',
                        'children' => [
                            ['name' => 'Castle Base'],
                            ['name' => 'CBD'],
                            ['name' => 'Chingaira'],
                            ['name' => 'Crocodile'],
                            ['name' => 'Gopal'],
                            ['name' => 'Lisapi'],
                            ['name' => 'Mabvazuva'],
                            ['name' => 'Magamba'],
                            ['name' => 'Mbuyanehanda'],
                            ['name' => 'Nyanga drive'],
                            ['name' => 'Silverpool'],
                            ['name' => 'Tsanzaguru'],
                            ['name' => 'Vengere'],
                            
                            ], 
                        ],
                        [
                        'name' => 'Shurugwi',
                        'icon' => 'fa-broadcast-tower',
                        'color' => 'F45B69',
                        'children' => [
                            ['name' => 'CBD'],
                            ['name' => 'Dark City'],
                            ['name' => 'Iron side'],
                            ['name' => 'Mukusha'],
                            ['name' => 'Peakmine'],
                            ['name' => 'Railway Block'],
                            ['name' => 'Tebekwe'],
                            ['name' => 'ZBS'],

                            ],
                        ],
                        [
                        'name' => 'Vic Falls',
                        'icon' => 'fa-hotel',
                        'color' => 'C2E812',
                        'children' => [
                            ['name' => 'Aerodrome'],
                            ['name' => 'CBD'],
                            ['name' => 'Chinotimba'],
                            ['name' => 'Mkhosana'],
                            ['name' => 'Suburbs'],
                            
                          ], 
                    ],
                    [
                        'name' => 'Zvishavane',
                        'icon' => 'fa-bell',
                        'color' => '32292F',
                        'children' => [
                            ['name' => 'Eastview'],
                            ['name' => 'Highlands'],
                            ['name' => 'Kandodo'],
                            ['name' => 'Mabula'],
                            ['name' => 'Maglas'],
                            ['name' => 'Makwasha'],
                            ['name' => 'Mandava'],
                            ['name' => 'Neil'],
                            ['name' => 'Novel'],
                            ['name' => 'Platinum Park'],
                         ],  
                    ],
                ],
            ],
        ];

        foreach ($areas as $area) {
            \App\Area::create($area);
        }
    }
}
